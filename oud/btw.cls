% Copyright (C) 1999-2015 TeXniCie A-Eskwadraat <hektex@a-eskwadraat.nl>
%
% This file is part of the A-Eskwadraat LaTeX collection and may be
% redistributed under the terms of version 2 of the GNU General Public License
% as published by the Free Software Foundation. See LICENSE file for details.
%
%% $Id: btw.cls 733 2015-02-23 14:38:43Z aldo $
%
% BTW LaTeX2e class by Baz
% Try #1 (july 25th, 1999)
% It's unbelievable, but it works!
% 2002 Thijs: toevoegen B.B.Cie. logo
% 2004 Schot: Wijzigen A-Es2 logo

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{btw}[1999/07/25 v1.0 By The Way goes LaTeX2e Class]
\RequirePackage[dutch]{babel}
\RequirePackage{epsfig,multicol,calc,amssymb,wrapfig,eurosym}
\LoadClass[twoside]{article}
%
%
% Margins ed
\paperheight=297mm
\voffset=-1in
\topmargin=6mm
\headheight=0pt
\headsep=0pt
\footskip=14pt
\newdimen\bottommargin \bottommargin=5mm
\setlength\textheight{\paperheight-\topmargin-\headheight-\headsep-\footskip-\bottommargin}
%
\paperwidth=210mm
\hoffset=-1in
\oddsidemargin=8.5mm
\evensidemargin=\oddsidemargin
\setlength\textwidth{\paperwidth-\oddsidemargin-\evensidemargin}
%
\parindent=0pt
%
\columnseprule=.4pt
\columnsep=20pt
\premulticols=0pt
\postmulticols=0pt
%
%
% De fonts
\font\large=ccsl10 at 80pt
\font\slogfont=ccslc9 scaled\magstep5
\font\aesfont=cmr10
\font\recyclefont=recycle scaled 250
\font\logofont=ccr8
\font\footerfont=ccr8
\font\twobf=cmssdc10 scaled\magstep2
\font\threebf=cmssdc10 scaled\magstep3
\font\tenrm=ccr10
\font\tenit=ccti10
\font\tensl=ccsl10
\font\tenbf=ccr10 % geen bf :-(
\font\sevenrm=ccr7
\font\seventt=cmtt7
\font\fiverm=ccr5
\font\sf=cmssdc10
\textfont0=\tenrm 
\scriptfont0=\sevenrm 
\scriptscriptfont0=\fiverm
\font\sc=cccsc10
%
\newbox\recycledbox 
\setbox\recycledbox\hbox{\recyclefont A} \ht\recycledbox=0pt
%
%
% De footers
\def\BTW@checknumpag{%
  \ifnum\count0>2 \ClassWarning{btw}{de BTW bestaat uit meer dan 2 kantjes!}\fi}
\def\@oddfoot{\BTW@checknumpag\hfil\hbox{\footerfont --- voorkant ---}\hss}
\def\@evenfoot{\BTW@checknumpag\hfil\hbox{\footerfont --- achterkant ---}\hss}
%
%
% wat user commando's (met dank aan Louis)
\def\nieuwekolom{\columnbreak}
\def\vulkolom{\null\par\vfill\par\columnbreak\null}
\def\lijn{\par\vskip1.5mm\hrule\vskip1.5mm}
\def\RemoveIndentation{{\setbox0=\lastbox}}
\def\ControlledIndentation{%
  \ifNeedIndent \ifneedindent
                \else \RemoveIndentation\needindenttrue\fi
  \else \ifneedindent \needindentfalse
        \else \RemoveIndentation
  \fi\fi}
\newif\ifNeedIndent
\newif\ifneedindent
\long\def\kop#1{\vskip 2mm\noindent{\twobf#1}\vskip 1mm plus 0.25mm\needindentfalse\noindent\ignorespaces}
\long\def\kops#1{\vskip 2mm\noindent{\twobf#1}\vskip 1mm plus 0.25mm\needindentfalse}
\everypar{\ControlledIndentation}
\newcount\agendapunt
\def\datum#1{\ifnum\agendapunt<1 \global\agendapunt=1 \else
  \par\noindent---\par\noindent\fi%
  {\twobf#1}\par\vskip 1mm\noindent\ignorespaces}

%\def\auteur#1{\par\noindent\hbox to\hsize{\hfil\sf #1\noindent\ignorespaces}} 
%auteur redefined by - GJC van Vliet
\def\auteur#1{\begin{flushright} \sf #1 \noindent\ignorespaces\end{flushright}}


\def\red{\auteur{De redactie}}
\def\agenda{\leftline{\threebf Agenda}\medskip\par\noindent\ignorespaces}
\def\advertentie{\centerline{\it(advertentie)}\par\noindent\ignorespaces}
\def\ingezonden{\centerline{\it(ingezonden mededeling)}\par\noindent\ignorespaces}
\def\btw{{\sl``By the way\dots''}}
\def\\{\hfil\break}
\edef\dots{$\ldots$}
\def\BBCie{\lower2pt\hbox{\epsfig{file=bbclogo.ps,height=12pt}}}
% 007 logo
\def\oo7{\lower2pt\hbox{\epsfig{file=..//logos//gunlogo.eps,height=9pt}}
           \ignorespaces}
% appeltje logo
\def\zeven{\lower2pt\hbox{\epsfig{file=..//zeven.eps,height=11pt}}}



%A-Eskwadraat met koppelteken en die op de juiste plekken wordt afgebroken - Schot
\def\AEs{\nobreak{\mbox{A--Es}kwadraat}}

%%
%
% Het BTW logo
\newbox\BTW@logo \setbox\BTW@logo\hbox{{\large By the way}}
\epsfxsize=\wd\BTW@logo
\epsfysize=\ht\BTW@logo \advance\epsfysize by\dp\BTW@logo
\setbox\BTW@logo\hbox{\epsfbox{btwlogo.ps}}
\newdimen\BTW@logowidth \BTW@logowidth=\wd\BTW@logo
%
%
% De user commando's (aanroepen _voor_ \begin{document})
\def\btwtitel#1{\global\def\BTW@titel{#1}}
 \def\BTW@titel{\ClassWarning{btw}{Geen titel gegeven}}
\def\btwdatum#1{\global\def\BTW@datum{#1}}
 \def\BTW@datum{\ClassWarning{btw}{Geen datum gegeven, ik gebruik \today}\today}
\def\btwjaargang#1{\global\def\BTW@jaargang{#1}}
 \def\BTW@jaargang{\ClassWarning{btw}{Geen jaargang gegeven}}
\def\btwnummer#1{\global\def\BTW@nummer{#1}}
 \def\BTW@nummer{\ClassWarning{btw}{Geen nummer gegeven}}
\def\btwoplage#1{\global\def\BTW@oplage{#1}}
 \def\BTW@oplage{\ClassWarning{btw}{Geen oplage gegeven}}
\def\btwredactie#1{\global\def\BTW@redactie{#1}}
 \def\BTW@redactie{\ClassWarning{btw}{Geen redactie gegeven}}
%
%
% Het colofon
\def\BTW@colofon{\vbox{\vskip5mm\hbox to36mm{\vbox{%
\hbox to36mm{\hss\epsfig{width=40mm,file=aes2logo-grijs.eps}}
\vskip5mm
\hskip2.8cm\box\recycledbox
\vskip-2mm
{\baselineskip=10pt
{\sl Redactie:}
\vskip 2pt\logofont
\BTW@redactie\par
}}\hss}}}
%
%
% De kop op pagina 1
\def\BTW@kop{%
  \null
  \vskip-10mm
  \hbox to\textwidth{\vbox{%
  \box\BTW@logo
  \vskip 1mm
  \hbox to\BTW@logowidth{\hfill{\slogfont\strut\BTW@titel}\hfill}
  \vskip 4mm
  \hbox to\BTW@logowidth{Datum: \BTW@datum\hfill Jaargang: \BTW@jaargang\hfill
    Nummer: \BTW@nummer\hfill Oplage: \BTW@oplage\ ex.}}
  \hss\BTW@colofon}
  \vskip 5mm
  \hrule width\textwidth height 2mm
  \vskip 5mm%
}

\AtBeginDocument{%
\parindent=1em
\begin{multicols}{3}[\BTW@kop]
}

\AtEndDocument{%
\end{multicols}
}

%
\endinput
