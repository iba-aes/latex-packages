% Copyright (C) 2006-2015 TeXniCie A-Eskwadraat <hektex@a-eskwadraat.nl>
%
% This file is part of the A-Eskwadraat LaTeX collection and may be
% redistributed under the terms of version 2 of the GNU General Public License
% as published by the Free Software Foundation. See LICENSE file for details.
%
%% $Id: bundel.cls 733 2015-02-23 14:38:43Z aldo $

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{bundel}[2007/03/05 v1.00 Eerstejaars-tentamenbundels]
\LoadClass[twoside, a4paper]{book}

\usepackage{a4wide}			% Breder!
\usepackage[dutch]{babel}		% Nederlandse taal
\RequirePackage{pdfpages}		% Dit is belangrijk: PDF-jes importeren
\RequirePackage{setspace}		% Environments voor variabele regelafstand
\RequirePackage{url}			% URL's, e-mailadressen
\RequirePackage{ifthen}			% Ifthenelses dus
\RequirePackage{aes}

% Uniforme A-Eskwadraatnaam.
\let\AEss\aesnaam

% Maak de unit length een milimeter.
\setlength{\unitlength}{1mm}

% Voorkom inspringing van paragrafen.
\setlength{\parindent}{0pt}

% Definieer de basis-pagestyle.
\pagestyle{empty}

% Maak maar paginanummering.
\includepdfset{pagecommand={\thispagestyle{plain}}}

% Dit is de lelijkste hek ooit. Niet gebruiken als er ook maar iets 
% anders moet worden geimporteerd.
\DeclareGraphicsRule{*}{pdf}{*}{}

% Maak maar een iets langere pagina, zodat de paginanummers niet tegen 
% de vragen aankruipen.
\addtolength{\footskip}{10mm}

% Doe wat dingen aan het begin van het document.
\AtBeginDocument{\setcounter{page}{3}\tableofcontents\cleardoublepage}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% En dan nu... de belangrijke commando's!!! %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Placeholders
\newcommand{\bnd@studiecode}{}
\newcommand{\bnd@vakcode}{}
\newcommand{\bnd@datum}{}

% Commando voor het begin van een studie. Argumenten:
%   1- TBC-code voor de studie; wi, na, ic, ik, of ws.
%   2- Volledige naam van de studie.
\newcommand{\studie}[2]{
	\renewcommand{\bnd@studiecode}{#1}
	\chapter{#2}
}

% Commando voor het begin van een vak. Argumenten:
%   1- TBC-vakcode voor het vak.
%   2- Volledige naam van het vak.
\newcommand{\vak}[2]{\clearpage
	\renewcommand{\bnd@vakcode}{#1}
	\addcontentsline{toc}{section}{#2}
}

% Commando voor een tentamen. Argumenten:
%   1- De datum van het tentamen.
%   2- Een asterisk als we van het tentamen een uitwerking online hebben, andersleeg laten.
\newcommand{\tentamen}[3][-]{\clearpage
	\renewcommand{\bnd@datum}{#2}
	\addcontentsline{toc}{subsection}{#2#3}
	\includepdf[pages=#1]{/home/cies/tbc/texsources/\bnd@studiecode/\bnd@vakcode/\bnd@vakcode.\bnd@datum.tent.pdf}
}
