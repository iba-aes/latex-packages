% Copyright (C) 2009-2014 TeXniCie A-Eskwadraat <hektex@a-eskwadraat.nl>

\NeedsTeXFormat{LaTeX2e}[1994/06/01]
\ProvidesClass{bo}[1999/09/22 v1.1 Standaard A--Eskwadraat BO-Notulen]

\ClassError{bo}{%
  Deze bo-class is oud en wordt niet meer onderhouden.\MessageBreak
  De TeXniCie adviseert je de notulen-class met de\MessageBreak
  bo-package te gebruiken%
}{Neem voor meer hulp contact op met de TeXniCie.}

\LoadClass{article}
\RequirePackage[dutch]{babel}
\RequirePackage{a4wide,tabularx,charter,supertabular,longtable,epsfig,aes}
\RequirePackage[utf8]{inputenc}

\hoffset-1in
\oddsidemargin25mm
\evensidemargin25mm
\textwidth160mm %210mm-25mm-25mm

\def\defaultjaar{2002 -- 2003}
\def\jaargang{\defaultjaar}
\newcount\@jaar

\@jaar=02
\def\jaar#1/#2#3{
	\def\jaargang{20#1 -- 20#2#3}
	\@jaar=#1
	}

\voffset-1in
\topmargin25mm
\headheight0pt
\headsep0pt
\textheight245mm % 297mm-25mm-25mm
%
\parindent0cm
\setlength\parskip{4pt plus 2pt minus 1pt}
\raggedbottom
%
\def\kop#1#2#3{%
  \parbox{.75\textwidth}{\bfseries\huge BO-notulen, #1\\A--Eskwadraatbestuur \jaargang\\ \hfill}\hfill
  \parbox{.25\textwidth}{\hfill
    \begin{tabular}{ll}
      volgnummer:&#3\\
      notulist:&#2\\
    \end{tabular}
  }
}
%
\setcounter{secnumdepth}{0}
%
\newcommand{\C}[1]{$\{$#1$\}^c$}
%
\newcommand{\kr}{$\mathbf{\times}$}
\newcommand{\kruis}{$\mathbf{\times}$}
\newcommand{\ander}[1]{$\mathbf{\rightarrow #1}$}
\newcommand{\ja}{$\mathbf{+}$} \let\plus\ja
\newcommand{\nee}{$\mathbf{-}$} \let\min\nee
\newcommand{\mwa}{$\mathbf{\pm}$}
\newcommand{\p}{$\mathbf{+}$} \let\plus\ja
\newcommand{\m}{$\mathbf{-}$} \let\min\nee
\newcommand{\mw}{$\mathbf{\pm}$}
\newcommand{\onbekend}{$\mathbf{?}$}
%
\def\axicie{\textsc{aXiCie}}
\def\BBCie{\lower2pt\hbox{\epsfig{file=bbclogo.ps,height=12pt}}}
\def\TeXniCie{\normalfont\textsf{\TeX niCie}}
\def\TBC{$\mathcal{T}
         \kern-.45em\lower.5ex\hbox{$\mathcal{B}$}
         \kern-.00em\mathcal{C}$\@\relax}
%
\newenvironment{oudetaken}{
  \def\Al{\bf Algemeen: && \\}

  % Hier laten staan wegen BC
  \ifnum\@jaar=02
    \def\J{\bf Jeroen: && \\}
    \def\JJ{\bf \mbox{Jan Jitse:} && \\}
    \def\Se{\bf Selma: && \\}
    \def\Sk{\bf Skadi: && \\}
    \def\H{\bf Hieke: && \\}
    \def\M{\bf Maarten: && \\}
  \fi

  %
  \ifnum\@jaar=03
    \def\Jef{\bf Jefrey: && \\}
    \def\JB{\bf Jan: && \\}
    \def\G{\bf Geert: && \\}
    \def\B{\bf Brigitte: && \\}
    \def\A{\bf Anneke: && \\}
    \def\M{\bf Maarten: && \\}
  \fi
  \ifnum\@jaar=04
    \def\CJ{\bf \mbox{Cor-Jan:} && \\}
    \def\L{\bf Lennart: && \\}
    \def\S{\bf Sophie: && \\}
    \def\J{\bf Jeroen: && \\}
    \def\Mi{\bf Mirjam: && \\}
    \def\Ma{\bf Marja: && \\}
  \fi
  \ifnum\@jaar=05
    \def\R{\bf Rianne: && \\}
    \def\S{\bf Sweitse: && \\}
    \def\L{\bf Lotte: && \\}
    \def\A{\bf Arjon: && \\}
    \def\P{\bf Peter: && \\}
    \def\B{\bf Bas: && \\}
  \fi
  \def\taak##1##2{& ##1 & ##2 \\}
  \footnotesize
  \begin{longtable}[l]{p{12mm}rl}
}{
  \end{longtable}
}

\newenvironment{taken}{
  \ifnum\@jaar=02
    \def\J{\bf Jeroen: && \\}
    \def\JJ{\bf \mbox{Jan~Jitse:} && \\}
    \def\Se{\bf Selma: && \\}
    \def\Sk{\bf Skadi: && \\}
    \def\H{\bf Hieke: && \\}
    \def\M{\bf Maarten: && \\}
  \fi
  \ifnum\@jaar=03
    \def\Jef{\bf Jefrey: && \\}
    \def\JB{\bf Jan: && \\}
    \def\G{\bf Geert: && \\}
    \def\B{\bf Brigitte: && \\}
    \def\A{\bf Anneke: && \\}
    \def\M{\bf Maarten: && \\}
  \fi
  \ifnum\@jaar=04
    \def\CJ{\bf \mbox{Cor-Jan:} && \\}
    \def\L{\bf Lennart: && \\}
    \def\S{\bf Sophie: && \\}
    \def\J{\bf Jeroen: && \\}
    \def\Mi{\bf Mirjam: && \\}
    \def\Ma{\bf Marja: && \\}
  \fi
  \ifnum\@jaar=05
    \def\R{\bf Rianne: && \\}
    \def\S{\bf Sweitse: && \\}
    \def\L{\bf Lotte: && \\}
    \def\A{\bf Arjon: && \\}
    \def\P{\bf Peter: && \\}
    \def\B{\bf Bas: && \\}
  \fi
  \def\Al{\bf Algemeen: && \\}
  \def\taak##1##2{& & ##2 \\}
  \footnotesize
  \begin{longtable}[l]{p{12mm}rl}
}{
  \end{longtable}
}

\newenvironment{oudeagenda}{
  \ifnum\@jaar=05
    \def\event##1##2##3##4##5{\texttt{##1} & ##3 & ##4  \\}
  \fi
  \ifnum\@jaar=04
    \def\event##1##2##3##4##5{\texttt{##1} & ##3 & ##4  \\}
  \fi
  \ifnum\@jaar=03
    \def\event##1##2##3##4##5{\texttt{##1} & ##3 & ##4  \\}
  \fi
  \ifnum\@jaar=02
    \def\event##1##2##3##4##5##6{\texttt{##1} & ##3 & ##4  \\}
  \fi
  \let\streep\hline
  \begin{tabular}{|l|l|l|}
  \hline
}{
  \hline
  \end{tabular}\medskip}

\newenvironment{agenda}{
  \ifnum\@jaar=05
    \def\event##1##2##3##4##5{\texttt{##1} & ##3 & ##4  \\}
  \fi
  \ifnum\@jaar=04
    \def\event##1##2##3##4##5{\texttt{##1} & ##3 & ##4  \\}
  \fi
  \ifnum\@jaar=03
    \def\event##1##2##3##4##5{\texttt{##1} & ##3 & ##4  \\}
  \fi
  \ifnum\@jaar=02
    \def\event##1##2##3##4##5##6{\texttt{##1} & ##3 & ##4  \\}
  \fi
  \let\streep\hline
  \begin{tabular}{|l|r|l|l|l|}
  \hline
}{
  \hline
  \end{tabular}\medskip}

\newenvironment{post}{
  \def\brief##1##2{\item[##1] ##2}
  \def\postin##1{
    \item[in:]\hfill\break\vspace*{-\baselineskip}
    \begin{description}
    ##1
    \end{description}
  }
  \def\postuit##1{
    \item[uit:]\hfill\break\vspace*{-\baselineskip}
    \begin{description}
    ##1
    \end{description}
  }
  \begin{description}
}{
  \end{description}
}

\newenvironment{cies}{
  \let\cie\item
  \def\mens##1{\medskip\item[\null\hskip-3em\normalfont\large\itshape ##1:]\nopagebreak}
  \def\streep{\vrule height2.5pt depth-2pt width1cm}
  \begin{description}
}{
  \end{description}
}

\newenvironment{rondvraag}{
  \let\vraag\item
  \begin{itemize}
}{
  \end{itemize}
}

\setlongtables

% redef list dimensions
%
\def\@listI{\leftmargin\leftmargini
  \parsep 0pt
  \partopsep2pt
  \topsep -\parskip
  \itemsep 0pt
  }
\let\@listi\@listI
\@listi
\def\@listii {\hfill\leftmargin\leftmarginii
  \labelwidth\leftmargini
  \advance\labelwidth-\labelsep
  \topsep0pt
  \parsep0pt
  \itemsep0pt}
\def\@listiii{\leftmargin\leftmarginiii
  \labelwidth\leftmarginiii
  \advance\labelwidth-\labelsep
  \topsep 0pt
  \parsep 0pt
  \partopsep 1pt
  \itemsep 0pt}
