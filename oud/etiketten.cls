% Copyright (C) 2011-2015 TeXniCie A-Eskwadraat <hektex@a-eskwadraat.nl>
%
% This file is part of the A-Eskwadraat LaTeX collection and may be
% redistributed under the terms of version 2 of the GNU General Public License
% as published by the Free Software Foundation. See LICENSE file for details.
%
%% $Id: etiketten.cls 733 2015-02-23 14:38:43Z aldo $

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{etiketten}[2011/11/01 Class om etiketten mee te maken op basis van who's who output]

\LoadClass[10pt,a4paper]{article} %Laad standaard article dingen

\usepackage[utf8]{inputenc}
\usepackage{forloop} % Voor de label offset
\usepackage{kix} %Voor Kixcode
\newcommand\kixcode[1]{\kix{\scantokens{#1}}}

%Defineer standaard afmetinegn n.a.v. bugg #4416
\usepackage[newdimens]{labels} %Set dimensions
\LabelCols=3 % 3 columns of labels
\LabelRows=7 % 7 rows of labels
\LeftPageMargin=7mm%
\RightPageMargin=7mm%
\TopPageMargin=16.2mm%
\BottomPageMargin=16.2mm%
\InterLabelColumn=0mm%Gap between columns of labels

%Definieer het eerste etiket dat gebruikt moet worden
\newcommand\labeloffset[1]{
	\newcounter{ct} 
	\forloop{ct}{0}{\value{ct} < #1}{
		\addresslabel{~}
	}
}

\newcommand\contact[5]{%
	\addresslabel{
	\mbox{#1} \\%Wat
	\mbox{t.a.v #2} \\%Wie
	#3 \\%Adres
	\vspace{2mm}
	\kixcode{#5}%kixcode
	\mbox{\tiny A--Eskwadraat, Princetonplein 5, 3584 CC Utrecht}}%Retouradres
}

\newcommand\persoon[9]{%
	\addresslabel{
	\mbox{#1 #3} \\ %Voornaam Achternaam
	#6 \\%Straat huisnummer
	#7 \\%Postcode plaats
	\vspace{2mm}
	\kixcode{#9}%Kixcode
	\mbox{\tiny A--Eskwadraat, Princetonplein 5, 3584 CC Utrecht}}%Retouradres
}

\newcommand\donateur[8]{%
	\addresslabel{#1 % Gehele adres
	\vspace{2mm}
	%Alles in 1, daarom geen kixcode mogelijk
	\mbox{\tiny A--Eskwadraat, Princetonplein 5, 3584 CC Utrecht}}%Retouradres
}