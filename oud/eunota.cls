% Copyright (C) 1994-2015 TeXniCie A-Eskwadraat <hektex@a-eskwadraat.nl>
%
% This file is part of the A-Eskwadraat LaTeX collection and may be
% redistributed under the terms of version 2 of the GNU General Public License
% as published by the Free Software Foundation. See LICENSE file for details.
%
%% $Id: eunota.cls 733 2015-02-23 14:38:43Z aldo $

\NeedsTeXFormat{LaTeX2e}[1994/06/01]
\ProvidesClass{eunota}[2006/02/16 v1.81 Standaard A-Eskwadraat nota]
\newcommand\@ptsize{}
\font\solletje=uusol
\RequirePackage{eurosym}
\RequirePackage[utf8]{inputenc}
\RequirePackage{aes}
%
% Deprecated-warning bij inladen en aan het eind van het document
\newcommand\w@@rschuwing{
  \ClassWarningNoLine{eunota}{%
    LET OP! De eunota-class wordt uitgefaseerd.\MessageBreak
    Gebruik het aesfactuur-package}%
}
\w@@rschuwing
\AtEndDocument{\w@@rschuwing}
%
% We maken wat if'jes voor de opties
%
\newif\ifned@brief\ned@brieftrue% true als nederlands (default)
\newif\ifclose@right\close@righttrue% true als closing rechts (default)
\newif\ifecht@streepje\echt@streepjefalse% true als streepje (niet default)
\newif\ifaes@brief\aes@brieftrue% true als A-Eskwadraatkopje enzo
\newif\ifgeld@terug\geld@terugfalse% true als negatieve factuur (niet default)
\newif\ifaes@herinnering\aes@herinneringfalse % true als een herrinneringsnota (niet default)
\newif\ifaes@boeken\aes@boekenfalse % true als het een boekenfactuur betreft (niet default)
%
% we hebben alleen maar A4, dus waarom niet zo?
%
\setlength\paperheight{297mm}
\setlength\paperwidth{210mm}
%
\setlength\overfullrule{0pt}
%
\DeclareOption{10pt}{\renewcommand\@ptsize{0}}
\DeclareOption{11pt}{\renewcommand\@ptsize{1}}
\DeclareOption{12pt}{\renewcommand\@ptsize{2}}
\DeclareOption{english}{\ned@brieffalse}
\DeclareOption{twoside}{\@latexerr{No 'twoside' layout for brief}\@eha}
\DeclareOption{oneside}{\@twosidefalse \@mparswitchfalse}
\DeclareOption{leqno}{\input{leqno.clo}}
\DeclareOption{fleqn}{\input{fleqn.clo}}
\DeclareOption{closingleft}{\close@rightfalse}
\DeclareOption{streepje}{\echt@streepjetrue}
\DeclareOption{geenaes}{\aes@brieffalse}
\DeclareOption{teruggave}{\geld@terugtrue}
\DeclareOption{herinnering}{\aes@herinneringtrue}
\DeclareOption{boeken}{\aes@boekentrue}
\ExecuteOptions{10pt,oneside,onecolumn}
\ProcessOptions
\input{size1\@ptsize.clo}
%
% specifieke A-Eskwadraat zooi
%
\newcount\j@@r\j@@r=\year
\loop\ifnum\j@@r>99\advance\j@@r-100\repeat
\ifnum\j@@r<10
  \ifnum\j@@r<0\j@@r=-\j@@r\def\jaar{\the\j@@r B.C.}
  \else\def\jaar{'0\the\j@@r}\fi
\else\def\jaar{'\the\j@@r}\fi
\def\empty{}
\def\commissienaam{Studievereniging \aesnaam}
\def\ecommissienaam{Study association \aesnaam}
\def\name#1{\def\fromname{#1}}
\def\email#1{\def\EMail{#1}}
\def\signature#1{\def\fromsig{#1}}
\def\uwk#1{\def\UwKenmerk{#1}}
\def\datum#1{\def\Datum{#1}}
\def\onsk#1{\def\OnsKenmerk{#1}}
\def\subject#1{\def\Subj{#1}}
\def\giro#1{\def\Giro{#1}}
\def\boekenrekening#1{\def\Boekenrekening{#1}}
\def\tav#1{\def\Tav{#1}}
\def\etav#1{\def\ETav{#1}}
\name{}
\email{bestuur@a-eskwadraat.nl}
\signature{}
\uwk{}
\datum{\vandaag}
\onsk{}
\subject{}
\tav{\commissienaam}
\etav{\ecommissienaam}
%
% zooi inladen als A-Eskwadraatbrief
%
\ifaes@brief
 \RequirePackage{epsfig}
 \RequirePackage{graphicx}
   \def\aeslogo{\includegraphics[width=4.5cm]{a-es}}
 \def\Giro{656927}
 \def\Boekenrekening{50 7122 690}
 %
 % lokale commissietekst...
 %
 \InputIfFileExists{brieftxt.sty}{}{}
 %
\fi
%
% begin nota ge#
%
\makeatletter
\parindent 0pt
\newcount\@totguldens  \newcount\@totcentjes
\newcount\@totalcentjes
\newcount\@guldens@tmp \newcount\@centjes@tmp
\newbox\@nota \newbox\bl@bl@
\newbox\g@h
\newdimen\@maxbreedte \@maxbreedte=0pt
\newdimen\@ombreedte  \@ombreedte=\textwidth
%
\def\doeniets{}
\def\puncd{.}%punctuatie duizendtallen
\def\puncc{,}%punctuatie centen
\def\@curr{$f$\quad}
%
\def\@putguldens#1{{\@guldens@tmp=#1
     \ifnum\@guldens@tmp>999
       \divide\@guldens@tmp by 1000
       \the\@guldens@tmp\puncd
       \multiply\@guldens@tmp by -1000
       \advance\@guldens@tmp by #1
       \ifnum\@guldens@tmp<100 0\ifnum\@guldens@tmp<10 0\fi\fi
     \else \ifnum\@guldens@tmp<-999
             \divide\@guldens@tmp by 1000
             \the\@guldens@tmp\puncd
             \multiply\@guldens@tmp by 1000
             \advance\@guldens@tmp by -#1
             \ifnum\@guldens@tmp<100 0\ifnum\@guldens@tmp<10 0\fi\fi
           \fi
     \fi
     \the\@guldens@tmp}}
\def\@putcentjes#1{{\@centjes@tmp=#1
	\puncc
         \ifnum\@centjes@tmp<10 0\fi
         \the\@centjes@tmp}}
%
\def\@@toklma#1#2#3{%
     \global\advance\@totguldens by #2
     \ifnum #2<0 \global\advance\@totcentjes by -#3
        \else \global\advance\@totcentjes by #3
	\fi
	{\setbox\g@h=\hbox{\@curr\@putguldens{#2}\@putcentjes{#3}}
		\ifdim\@maxbreedte<\wd\g@h \global\@maxbreedte=\wd\g@h\fi}
	}
\def\@@toklmb#1#2#3{\setbox\@nota=\vbox{\box\@nota
            \hbox to \textwidth{\parbox[b]{\@ombreedte}{%
		\multiply\baselineskip by 85
		\divide\baselineskip by 100
		#1}%
                 \hfill\hbox to\@maxbreedte{\@curr\hfill\@putguldens{#2}%
                 \@putcentjes{#3}}}
		\smallskip}}
%
\def\flauw#1\end#2{\begingroup
	\def\post##1\e##2.##3 {%
		\@@toklma{##1}{##2}{##3}}
	\def\totaal{%
            \@totalcentjes=\@totguldens
            \multiply\@totalcentjes by 100
            \advance\@totalcentjes by \@totcentjes
            \ifnum\@totalcentjes < 0
              \global\@totguldens=\@totalcentjes
              \global\divide\@totguldens by 100
              \global\@totcentjes=\@totguldens
              \global\multiply\@totcentjes by 100
              \@totalcentjes=-\@totalcentjes
              \global\advance\@totcentjes by \@totalcentjes
            \else
              \global\@totguldens=\@totalcentjes
              \global\divide\@totguldens by 100
              \global\@totcentjes=\@totguldens
              \global\multiply\@totcentjes by -100
              \global\advance\@totcentjes by \@totalcentjes
            \fi
	    \setbox\bl@bl@=\hbox{\@curr\@putguldens{\the\@totguldens}%
		\@putcentjes{\the\@totcentjes}}%
		\ifdim\@maxbreedte<\wd\bl@bl@ \global\@maxbreedte=\wd\bl@bl@
		\fi
             	\global\advance\@ombreedte by-\@maxbreedte
		}%end deftotaal
            {\setbox0=\vbox{#1}}
	    \def\post##1\e##2.##3 {\@@toklmb{##1}{##2}{##3}}
   	\def\totaal{%
            \box\@nota
            \bigskip
            \hbox to \textwidth{\hfill\hbox to \@maxbreedte{\hrulefill}}
            \hbox to \textwidth{\parbox[b]{\@ombreedte}{\Totaal}%
                 	\hfill\hbox to\@maxbreedte{\@curr\hfill
   			\@putguldens{\the\@totguldens}%
			\@putcentjes{\the\@totcentjes}}}
		\typeout{Totaal: \the\@totguldens,\the\@totcentjes}
		}%end deftotaal
            #1\endgroup\end{#2}}
%
%
\ifned@brief % Nederlandse brief
  \AtEndOfClass{\RequirePackage[dutch]{babel}}
  \def\bijlagename{Bijlage\ifnum\c@blgcnt>1 n\fi}
  \def\bijlagetext{Bijlage}
  \def\datumlabel{Datum}
  \def\uwkenmerklabel{Uw kenmerk}
  \def\onskenmerklabel{Ons kenmerk}
  \def\onderwerplabel{Onderwerp}
  \def\omschrijving{Omschrijving}
  \def\factuur{
    \ifaes@herinnering Herinneringsfactuur
    \else Factuur\fi
  }
  \def\vandaag{\number\day\space \ifcase\month\or
    januari\or februari\or maart\or april\or mei\or juni\or
    juli\or augustus\or september\or oktober\or november\or december\fi
    \space \jaar}
  \let\today=\vandaag
  \def\adres{%
      \vtop {\hbox{\ciefont\bfseries\fontsize{10}{12}\selectfont\commissienaam}
     	    \vskip 5mm
     	    \hbox{\ciefont Buys Ballot Lab., kamer 520}
            \hbox{\ciefont Princetonplein 5}
            \hbox{\ciefont Uithof, Utrecht}
            \vskip 3mm
            \hbox{\ciefont Telefoon (030) 253\,44\,99}
            \hbox{\ciefont Fax (030) 253\,57\,87}
            \vskip 3mm
            \hbox{\ciefont K.v.K.\ Utrecht 40479641}}}
  \def\pobox{Postbus}
  \def\betalekreng{\normalsize \ifgeld@terug%
		Let op! Het negatieve totaalbedrag op deze factuur geeft aan dat het
		een factuur betreft in \textit{uw voordeel}. Het bovenstaande bedrag
		is inmiddels overgemaakt op het door u opgegeven bank- of
		girorekeningnummer.%
		\else %
			Wij brengen geen BTW in rekening daar A-Eskwadraat een vereniging
			zonder winstoogmerk is.
			Wij verzoeken u het totaalbedrag binnen 21 dagen na dagtekening
			over te maken op
			\ifaes@boeken%
				rekeningnummer \Boekenrekening\ %
			\else%
				gironummer \Giro\ %
			\fi%
			t.n.v.\ A-Eskwadraat o.v.v.\ \ifx\OnsKenmerk\empty \Tav. \else \OnsKenmerk.
		\fi\fi}
  \def\puncd{.}%punctuatie duizendtallen
  \def\puncc{,}%punctuatie centen
  \def\@curr{\euro\quad}
  \def\Totaal{Totaal}
\else % Engelse brief
  \AtEndOfClass{\RequirePackage[english]{babel}}
  \def\bijlagename{Enclosure\ifnum\c@blgcnt>1 s\fi}
  \def\bijlagetext{Enclosure}
  \def\datumlabel{Date}
  \def\uwkenmerklabel{Your reference}
  \def\onskenmerklabel{Our reference}
  \def\onderwerplabel{Subject}
  \def\omschrijving{Description}
  \def\factuur{Invoice}
  \ifnum\year<0
  	\def\vandaag{\ifcase\month\or
    	January\or February\or March\or April\or May\or June\or July\or
    	August\or September\or October\or November\or December\fi
    	\space\number\day,\space \number-\year\ B.C.}
    \else
  	\def\vandaag{\ifcase\month\or
    	January\or February\or March\or April\or May\or June\or July\or
    	August\or September\or October\or November\or December\fi
    	\space\number\day,\space \number\year}
    \fi
  \let\today=\vandaag
  \def\adres{%
      \vtop {\hbox{\ciefont\bfseries\fontsize{10}{12}\selectfont\ecommissienaam}
     	    \vskip 5mm
     	    \hbox{\ciefont Buys Ballot Lab., room 520}
            \hbox{\ciefont Princetonplein 5}
            \hbox{\ciefont Uithof, Utrecht}
            \vskip 3mm
            \hbox{\ciefont Telephone +31 30 253\,44\,99}
            \hbox{\ciefont Fax +31 30 253\,57\,87}
            \vskip 3mm
            \hbox{\ciefont C.\,of\,C.\ Utrecht 40479641}}}
  \def\pobox{PO box}
  \def\betalekreng{%
        We ask You to deposit the total amount within 30 days
        with \Giro\ (Postbank), `Fiscus studievereniging
        \aesnaam,
	\ifx\OnsKenmerk\empty \ETav'. \else \OnsKenmerk'.\fi}
  \def\puncd{,}%punctuatie duizendtallen
  \def\puncc{.}%punctuatie centen
  \def\Totaal{Total}
  \def\@curr{\euro\quad}
\fi
%
\setlength\lineskip{1\p@}
\setlength\normallineskip{1\p@}
\renewcommand\baselinestretch{}
\setlength\parskip{4mm plus 1pt minus .6mm}
\setlength\parindent{0\p@}
\@lowpenalty   51
\@medpenalty  151
\@highpenalty 301
\setlength\headheight{12\p@}
\setlength\headsep   {12\p@}
\setlength\footskip{25\p@}
\if@compatibility
  \setlength\maxdepth{4\p@}
\else
  \setlength\maxdepth{.5\topskip}
\fi
\setlength\@maxdepth\maxdepth
\setlength\textwidth{155mm}
\setlength\textheight{265mm}
\setlength{\@tempdima}{\paperwidth}
\addtolength{\@tempdima}{-2in}
\addtolength{\@tempdima}{-\textwidth}
\setlength\oddsidemargin   {.5\@tempdima}
\setlength\evensidemargin  {\oddsidemargin}
\setlength\marginparwidth  {90\p@}
\setlength\marginparsep {11\p@}
\setlength\marginparpush{5\p@}
\setlength\topmargin{-1.8cm}
\setlength\footnotesep{12\p@}
\setlength{\skip\footins}{10\p@ \@plus 2\p@ \@minus 4\p@}
\voffset-1.8cm
%
% dit zijn de orginele ps@empty en ps@plain
%
\def\ps@empty{%
      \let\@oddfoot\@empty\let\@oddhead\@empty
      \let\@evenfoot\@empty\let\@evenhead\@empty}

\def\ps@plain{%
      \let\@oddhead\@empty
      \def\@oddfoot{\rmfamily\hfil\thepage\hfil}%
      \def\@evenfoot{\rmfamily\hfil\thepage\hfil}}
%
% dit is een andere ps@headings
%
\if@twoside
  \def\ps@headings{%
      \let\@oddfoot\@empty\let\@evenfoot\@empty
      \def\@oddhead{\slshape\headtoname{}: \ignorespaces\toname
                    \hfil \pagename{}: \thepage}%
      \let\@evenhead\@oddhead
\else
  \def\ps@headings{%
      \let\@oddfoot\@empty
      \def\@oddhead{\slshape\headtoname{}: \ignorespaces\toname
                    \hfil \pagename{}: \thepage}}
\fi
%
% dit is een andere ps@firstpage (zodat wij het A-Eskwadraatadres
% veilig onderaan kunnen zetten (moet ook in de `foot' kunnen, maar dit
% lijkt makkelijker...))
\def\ps@firstpage{%
     \let\@oddhead\@empty \let\@oddfoot\@empty
   }% end \ps@firstpage
%
\def\makelabels{%
  \AtBeginDocument{%
     \let\@startlabels\startlabels
     \let\@mlabel\mlabel
     \if@filesw
       \immediate\write\@mainaux{\string\@startlabels}\fi}%
  \AtEndDocument{%
     \if@filesw\immediate\write\@mainaux{\string\clearpage}\fi}}
\@onlypreamble\makelabels
\newenvironment{nota}[1]
  {\def\aan{#1}\newpage
    \if@twoside \ifodd\c@page
                \else\thispagestyle{empty} \hbox{}\newpage\fi
    \fi
    \c@page\@ne
    \interlinepenalty=200 % smaller than the TeXbook value
    \@processto{\leavevmode\ignorespaces #1}\opening}
  {\stopletter\@@par\pagebreak\@@par
    \if@filesw
      \begingroup
        \let\\=\relax
        \def\protect##1{\string##1\space}%
          \immediate\write\@auxout
            {\string\@mlabel{\returnaddress}{\toname\\\toaddress}}%
      \endgroup
    \fi\bijl@gen
% toegevoegd 30-03-99 door FvL om meerdere nota's met 'e'en bestand te maken
    \global\@totguldens=0\global\@totcentjes=0\global\@totalcentjes=0%
    \global\@maxbreedte=0pt\global\@ombreedte=\textwidth}
\long\def\@processto#1{\@xproc #1\\@@@\ifx\toaddress\@empty
    \else \@yproc #1@@@\fi}
\long\def\@xproc #1\\#2@@@{\def\toname{#1}\def\toaddress{#2}}
\long\def\@yproc #1\\#2@@@{\def\toaddress{#2}}
\def\stopbreaks{\interlinepenalty \@M
   \def\par{\@@par\nobreak}\let\\=\@nobreakcr
   \let\vspace\@nobreakvspace}
\def\@nobreakvspace{\@ifstar{\@nobreakvspacex}{\@nobreakvspacex}}

\def\@nobreakvspacex#1{\ifvmode\nobreak\vskip #1\relax\else
               \@bsphack\vadjust{\nobreak\vskip #1}\@esphack\fi}

\def\@nobreakcr{\vadjust{\penalty\@M}\@ifstar{\@xnewline}{\@xnewline}}
\def\startbreaks{\let\\=\@normalcr
   \interlinepenalty 200\def\par{\@@par\penalty 200\relax}}

\newdimen\longindentation
\longindentation=.5\textwidth
\newdimen\indentedwidth
\indentedwidth=\textwidth
\advance\indentedwidth -\longindentation
%
\def\labelfont{\normalfont\sffamily\fontsize{8.5}{11}\selectfont}
\def\inhfont{\normalfont\fontsize{10}{12}\selectfont}
\def\ciefont{\normalfont\sffamily\fontsize{8.5}{12}\selectfont}
%
% Dit is de opening
%
\def\opening{\thispagestyle{firstpage}%
   \vtop to 0pt{\vskip -2mm\hbox to 0pt{% de oude positie..
	\hskip 16mm{\includegraphics{uboogje}}\hss}
	\vss}
   {\vtop to 0pt{\parskip \z@
     \vbox to 36mm{\vskip 6mm\hbox{%
      \hbox to 40mm{% ons kenmerk / datum
       \vtop{\hbox{\labelfont\uwkenmerklabel}%
             \hbox{\inhfont\UwKenmerk}%
             \vskip 9mm
             \hbox{\labelfont\datumlabel}%
             \hbox{\inhfont\Datum}
            }
       \hfill
      }
      \hbox to 80mm{% e-mail / doorkiesnummer / onderwerp
       \vtop{\hbox{\labelfont\hbox to 40mm{\onskenmerklabel\hfill}E-mail}
             \hbox{\inhfont\hbox to 40mm{\OnsKenmerk\hfill}\EMail}
             \vskip 9mm
             \hbox{\labelfont\onderwerplabel}
             \hbox{\inhfont\Subj}
       }
       \hfill
      }
     \hbox to 42mm {% bezoekadres
        \adres\hss}
    }% end \hbox kenmerken
    \vss
    }
    \hbox{\labelfont A--Eskwadraat, Princetonplein 5, 3584 CC Utrecht, NL}
    \vskip 20pt
    \vbox{\large\aan}
    \vbox to 0pt{\vskip-0.6cm\hbox to \textwidth{\hfill\aeslogo}\vss}
    \vskip 2cm
    \hbox to \textwidth {\Huge\it \factuur\hfill}
    \vskip 17.5cm
    \parbox{\textwidth}{\ciefont\tt \betalekreng}
   \vss}% end group (voor de parskip \z@)
}
   \vskip 7.0cm
   \hbox{\ciefont\Large \omschrijving:}\vskip-2mm\tt
   \bigskip
   \flauw
}% end opening
%
% Dit is de closing
%
\long\def\closing#1{\par\nobreak\vspace{6mm}
   \stopbreaks
   \noindent
   \ifclose@right
      \hspace*{\longindentation}%
   \fi
   \parbox{\indentedwidth}{%
      \raggedright
      \ignorespaces #1\\[2cm]
      \ifx\fromsig\empty
         \fromname
      \else
         \fromsig
      \fi
      \strut
   }% end \parbox
\par}
%
%
\medskipamount=\parskip
\def\cc#1{\par\noindent
\parbox[t]{\textwidth}{\@hangfrom{\reset@font\rm \ccname: }%
                       \ignorespaces #1\strut}\par}

\def\bijl@firstitem[#1]#2\item{%
  \def\bijl@{#1} \def\bijl@@{\relax}
  \ifx\bijl@\bijl@@  \def\item{}
  \else
    \expandafter\gdef\csname bijl@gen\the\c@blgcnt\endcsname%
    {\global\advance\c@blgcnt1\clearpage\thispagestyle{empty}
    {\Large\bfseries\bijlagetext~\the\c@blgcnt: #2}%
    \bigskip\par\input{#1}\csname bijl@gen\the\c@blgcnt\endcsname}%
    \global\advance\c@blgcnt1
  \fi\item}
\def\bijl@secitem[#1]#2\item{%
  \def\bijl@{#1} \def\bijl@@{\relax}
  \ifx\bijl@\bijl@@  \def\item{}
  \else
    \global\advance\c@blgcnt1
    \hbox{\kern3em\llap{\the\c@blgcnt.}\kern.7em\ignorespaces#2}\par
  \fi\item}
\def\@bijlagen#1\end#2{\begingroup\parskip0pt\parindent0pt
  \newcount\c@blgcnt
  \def\item{\@ifnextchar [{\bijl@firstitem}{\bijl@firstitem[null]}}
  #1\item[\relax]\item
  \def\item{\@ifnextchar [{\bijl@secitem}{\bijl@secitem[null]}}
  \vfill
  \bijlagename:\par\c@blgcnt=0\relax#1\item[\relax]\item\endgroup\end{#2}}
\def\bijl@gen{\newcount\c@blgcnt\csname bijl@gen0\endcsname}
\expandafter\def\csname bijl@gen0\endcsname{}
\def\geenbijlagen{\def\bijl@gen{}}
\newenvironment{bijlagen}{\@bijlagen}{}

\def\encl#1{\par\noindent
\parbox[t]{\textwidth}{\@hangfrom{\reset@font\rm \enclname: }%
                       \ignorespaces #1\strut}\par}
\def\ps{\par\startbreaks}
\def\stopletter{}
\def\returnaddress{}
\newcount\labelcount
\def\startlabels{\labelcount\z@
\pagestyle{empty}%
\let\@texttop\relax
\topmargin -50\p@
\headsep \z@
\oddsidemargin -35\p@
\evensidemargin -35\p@
\textheight 10in
\@colht\textheight  \@colroom\textheight \vsize\textheight
\textwidth 550\p@
\columnsep 25\p@
\ifcase \@ptsize\relax
  \normalsize
 \or
  \small
 \or
  \footnotesize
 \fi
\baselineskip \z@
\lineskip \z@
\boxmaxdepth \z@
\parindent \z@
\twocolumn\relax}
\let\@startlabels=\relax
\def\mlabel#1#2{\setbox0\vbox{\parbox[b]{3.6in}%
                                        {\strut\ignorespaces #2}}%
         \vbox to 2in{\vss \box0 \vss}}
\let\@mlabel=\@gobbletwo
\setlength\leftmargini  {2.5em}
\setlength\leftmarginii  {2.2em}
\setlength\leftmarginiii {1.87em}
\setlength\leftmarginiv  {1.7em}
\setlength\leftmarginv  {1em}
\setlength\leftmarginvi {1em}
\setlength\leftmargin    {\leftmargini}
\setlength  \labelsep  {5\p@}
\setlength  \labelwidth{\leftmargini}
\addtolength\labelwidth{-\labelsep}
\setlength\partopsep{0\p@}
\@beginparpenalty -\@lowpenalty
\@endparpenalty   -\@lowpenalty
\@itempenalty     -\@lowpenalty
\def\@listI{\setlength\leftmargin{\leftmargini}
            \setlength\parsep {0\p@}%
            \setlength\topsep {.4em}%
            \setlength\itemsep{.4em}}
\let\@listi\@listI
\@listi
\def\@listii {\setlength  \leftmargin{\leftmarginii}%
              \setlength  \labelwidth{\leftmarginii}%
              \addtolength\labelwidth{-\labelsep}}
\def\@listiii{\setlength  \leftmargin{\leftmarginiii}%
              \setlength  \labelwidth{\leftmarginiii}%
              \addtolength\labelwidth{-\labelsep}%
              \setlength  \topsep    {.2em}%
              \setlength  \itemsep   {\topsep}}
\def\@listiv {\setlength  \leftmargin{\leftmarginiv}%
              \setlength  \labelwidth{\leftmarginiv}%
              \addtolength\labelwidth{-\labelsep}}
\def\@listv  {\setlength  \leftmargin{\leftmarginv}%
              \setlength  \labelwidth{\leftmarginv}%
              \addtolength\labelwidth{-\labelsep}}
\def\@listvi {\setlength  \leftmargin{\leftmarginvi}%
              \setlength  \labelwidth{\leftmarginvi}%
              \addtolength\labelwidth{-\labelsep}}
\renewcommand\theenumi{\arabic{enumi}}
\renewcommand\theenumii{\alph{enumii}}
\renewcommand\theenumiii{\roman{enumiii}}
\renewcommand\theenumiv{\Alph{enumiv}}
\newcommand\labelenumi{\theenumi.}
\newcommand\labelenumii{(\theenumii)}
\newcommand\labelenumiii{\theenumiii.}
\newcommand\labelenumiv{\theenumiv.}
\renewcommand\p@enumii{\theenumi}
\renewcommand\p@enumiii{\theenumi(\theenumii)}
\renewcommand\p@enumiv{\p@enumiii\theenumiii}
\newcommand\labelitemi{$\m@th\bullet$}
\newcommand\labelitemii{\normalfont\bfseries --}
\newcommand\labelitemiiii{$\m@th\ast$}
\newcommand\labelitemiv{$\m@th\cdot$}
\newenvironment{description}
               {\list{}{\labelwidth\z@ \itemindent-\leftmargin
                        \let\makelabel\descriptionlabel}}
               {\endlist}
\newcommand\descriptionlabel[1]{\hspace\labelsep
                                \normalfont\bfseries #1}

\newenvironment{verse}
               {\let\\=\@centercr
                \list{}{\setlength\itemsep{\z@}%
                        \setlength\itemindent{-15\p@}%
                        \setlength\listparindent{\itemindent}%
                        \setlength\rightmargin{\leftmargin}%
                        \addtolength\leftmargin{15\p@}}%
                \item[]}
               {\endlist}
\newenvironment{quotation}
               {\list{}{\setlength\listparindent{1.5em}%
                        \setlength\itemindent{\listparindent}%
                        \setlength\rightmargin{\leftmargin}}%
                \item[]}
               {\endlist}
\newenvironment{quote}
               {\list{}{\setlength\rightmargin{\leftmargin}}%
                \item[]}
               {\endlist}

\setlength\arraycolsep{5\p@}
\setlength\tabcolsep{6\p@}
\setlength\arrayrulewidth{.4\p@}
\setlength\doublerulesep{2\p@}
\setlength\tabbingsep{\labelsep}
\skip\@mpfootins = \skip\footins
\setlength\fboxsep{3\p@}
\setlength\fboxrule{.4\p@}
\renewcommand\theequation{\arabic{equation}}
%
% *************************
% dit komt uit newlfont.sty
% *************************
%
\let\rm\rmfamily
\let\sf\sffamily
\let\tt\ttfamily
\let\bf\bfseries
\let\sl\slshape
\let\sc\scshape
\let\it\itshape
\DeclareRobustCommand\em{%
  \@nomath\em
  \ifdim \fontdimen\@ne\font >\z@\upshape \else \itshape \fi}
\let\mediumseries\mdseries
\let\normalshape\upshape
\def\@setfontsize#1#2#3{\@nomath#1%
    \ifx\protect\relax
      \let\@currsize#1%
    \fi
    \fontsize{#2}{#3}\selectfont}
\let\math@bgroup\bgroup
\def\math@egroup#1{#1\egroup}
\let \@@math@bgroup \math@bgroup
\let \@@math@egroup \math@egroup
\def\not@math@alphabet#1#2{%
   \relax
   \ifmmode
     \@latex@error{Command \noexpand#1invalid in math mode}%
        {%
         Please
         \ifx#2\relax
            define a new math alphabet^^J%
            if you want to use a special font in math mode%
          \else
            use the math alphabet #2instead of
            the #1command%
         \fi
         .
        }%
   \fi}
\let\pcal\@undefined
\let\cal\mathcal
\let\pmit\@undefined
\let\mit\mathnormal
%
% *******************
% dit is latexsym.sty
% *******************
%
\ifx\symlasy\undefined \else
  \wlog{Package latexsym: nothing to set up^^J}%
  \fi
  \DeclareSymbolFont{lasy}{U}{lasy}{m}{n}
  \SetSymbolFont{lasy}{bold}{U}{lasy}{b}{n}
  \let\mho\undefined            \let\sqsupset\undefined
  \let\Join\undefined           \let\lhd\undefined
  \let\Box\undefined            \let\unlhd\undefined
  \let\Diamond\undefined        \let\rhd\undefined
  \let\leadsto\undefined        \let\unrhd\undefined
  \let\sqsubset\undefined

  \DeclareMathSymbol\mho     {0}{lasy}{"30}
  \DeclareMathSymbol\Join    {3}{lasy}{"31}
  \DeclareMathSymbol\Box     {0}{lasy}{"32}
  \DeclareMathSymbol\Diamond {0}{lasy}{"33}
  \DeclareMathSymbol\leadsto {3}{lasy}{"3B}
  \DeclareMathSymbol\sqsubset{3}{lasy}{"3C}
  \DeclareMathSymbol\sqsupset{3}{lasy}{"3D}
  \DeclareMathSymbol\lhd     {3}{lasy}{"01}
  \DeclareMathSymbol\unlhd   {3}{lasy}{"02}
  \DeclareMathSymbol\rhd     {3}{lasy}{"03}
  \DeclareMathSymbol\unrhd   {3}{lasy}{"04}
%
%
%
\renewcommand\footnoterule{%
  \kern-\p@
  \hrule \@width .4\columnwidth
  \kern .6\p@}
\long\def\@makefntext#1{%
    \noindent
    \hangindent 5\p@
    \hbox to5\p@{\hss\@makefnmark}#1}
%
%
\setlength\columnsep{10\p@}
\setlength\columnseprule{0\p@}
%
\smallskipamount=.5\parskip \medskipamount=\parskip
\bigskipamount=1.2\parskip
\parskip 3mm plus 1pt minus .6mm
%
\pagestyle{plain}
\pagenumbering{arabic}
\raggedbottom
%\def\@texttop{\ifnum\c@page=1\vskip \z@ plus.00006fil\relax\fi}
\let\@texttop=\relax
\onecolumn
\makeatother
\endinput
