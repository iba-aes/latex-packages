% Copyright (C) 2008-2014 TeXniCie A-Eskwadraat <hektex@a-eskwadraat.nl>
%
% This file is part of the A-Eskwadraat LaTeX collection and may be
% redistributed under the terms of version 2 of the GNU General Public License
% as published by the Free Software Foundation. See LICENSE file for details.
%
%% $Id: aesbrief.cls 807 2017-04-04 10:53:24Z aldo $

%Werking en nut: Maakt de brief, duh

\NeedsTeXFormat{LaTeX2e}[1999/06/01]

%% BESTANDSINFORMATIE
\newcommand\filename{aesbrief}
\newcommand\filedate{2015/10/15}
\newcommand\fileversion{4.42}

\ProvidesClass{\filename}[\filedate\space v\fileversion\space A-Eskwadraat brief class]

%% ARTICLE LADEN
\LoadClass{article}

%% PAGINA LAY-OUT

\usepackage[a4paper, left=21mm, right=21mm, top=117mm, bottom=20mm]{geometry}


%Toch even voor de zekerheid de code die nu door de geometry package wordt gedaan. Is het 2018? Verwijder dit. 
%\setlength\paperwidth{210mm}
%\setlength\paperheight{297mm}
%\setlength\pdfpagewidth{210mm}
%\setlength\pdfpageheight{297mm}
%
%\setlength\hoffset{-1in}
%\setlength\voffset{-1in}
%
%\setlength\oddsidemargin{21mm}
%\setlength\evensidemargin{21mm}
%\setlength\topmargin{117mm} % 117mm is noodzakelijk voor een eventuele tweede pagina (zie ook \aes@briefhoofd)
                            % hiermee komt de tekst van pagina 2 precies op de hoogte van de tekst van pagina 1
%\setlength\headheight{0mm}
%\setlength\headsep{0mm}
%
\setlength\parindent{0pt}
\setlength\parskip{\baselineskip}
%
\setlength\textwidth{165mm}
\setlength\textheight{160mm} % = 297 - 20 - 117 (hoogte A4 - bottommarge - briefhoofd-hoogte)
%
% speciale footer-pagestyle, als de footer-optie is gegeven
\newcommand\ps@footer{
        \renewcommand\@oddhead{}
        \renewcommand\@evenhead{}
        \renewcommand\@oddfoot{%
        \hspace{-1cm}%
        \begin{minipage}[c]{0.6\textwidth}
        \begin{flushleft}
        \aes@fontfooter\TAALverenigingfooter%
        \end{flushleft}
        \end{minipage}
        \ifthenelse{\boolean{aes@optkleur}}{}{%
        \hfill%
        \begin{minipage}[b][1mm][t]{4.6cm}%
                \vspace*{-17mm}\par % Het UU-logo is op het "echte" briefpapier ook 5cm breed
                \includegraphics[scale=.65]{logos/UU-logo2011_ZWART}          
        \end{minipage}
        }       %
        }
        \let\@evenfoot\@oddfoot
}

% \TeX definitie van \raggedright, de standaarddefinitie werkt niet
\renewcommand\raggedright{\rightskip\z@ plus2em
\spaceskip.3333em
\xspaceskip.5em\relax}

%% PACKAGES LADEN
\RequirePackage{aes}
\RequirePackage{graphicx}
\RequirePackage{ifthen} % ifthen is nu nodig vanwege \newboolean verderop
\RequirePackage{kix}
\RequirePackage{xspace}
% babel staat onderaan OPTIES, vanwege \PassOptionsToPackage aldaar


%% DIVERSE BOOLEANS
\newboolean{aes@optdebug}
\newboolean{aes@optkleur}
\newboolean{aes@optfooter}
\newboolean{aes@ontour}
\newboolean{aes@betadag}

\RequirePackage{aestaal}

%% OPTIES
% Laat aestaal de taalopties klaarzetten.
\DeclareTaalOpties

\DeclareOption{debug}{\setboolean{aes@optdebug}{true}}
\DeclareOption{nodebug}{\setboolean{aes@optdebug}{false}}
\DeclareOption{kleur}{\setboolean{aes@optkleur}{true}}
\DeclareOption{nokleur}{\setboolean{aes@optkleur}{false}}
\DeclareOption{geenkleur}{\setboolean{aes@optkleur}{false}}
\DeclareOption{footer}{\setboolean{aes@optfooter}{true}}
\DeclareOption{nofooter}{\setboolean{aes@optfooter}{false}}
\DeclareOption{ontour}{\setboolean{aes@ontour}{true}}
\DeclareOption{betadag}{\setboolean{aes@betadag}{true}}
\DeclareOption*{%
        \PassOptionsToClass{\CurrentOption}{article}}

\ExecuteOptions{nodebug,nokleur,footer}
\ProcessOptions*  % *, dus verwerken in door gebruiker opgegeven volgorde (belangrijk voor talen)

% Laat aestaal babel met de gekozen taalopties (of dutch, indien niks opgegeven) laden.
\LaadBabel

%
%
%% LETTERTYPES
\newcommand\aes@fontheader{\fontsize{8}{11}\selectfont\sffamily}
\newcommand\aes@fonttext{\fontsize{9}{12}\selectfont}
\newcommand\aes@fontnaam{\fontsize{10}{12}\selectfont\sffamily\bfseries}
\newcommand\aes@fontfooter{\fontsize{9.5}{11}\selectfont\slshape}
\newcommand\aes@fontadres{\fontsize{13}{15}\selectfont}
%
\renewcommand\familydefault{\rmdefault}
\renewcommand\seriesdefault{m}
\renewcommand\shapedefault{n}
%
\renewcommand\rmdefault{ppl}
\renewcommand\sfdefault{phv}
\renewcommand\ttdefault{cmtt}
%
\renewcommand\bfdefault{b}
\renewcommand\mddefault{m}
%
\renewcommand\itdefault{it}
\renewcommand\sldefault{sl}
\renewcommand\scdefault{sc}
\renewcommand\updefault{n}
%
% geen paginanummers enzo
\pagestyle{empty}


%% GEBRUIKERSCOMMANDO'S

% Scoping is in dit geval best hip: instellingen binnen de brief-environment
% gelden voor alleen de huidige brief. Instellingen buiten de brief-environment
% gelden voor alle brieven vanaf dat moment.

%%%% lay-out parameters
\newcommand\welkleur{\setboolean{aes@optkleur}{true}}
\newcommand\geenkleur{\setboolean{aes@optkleur}{false}}

%%%% INHOUDSPECIFIEKE COMMANDO'S
% organisatie/afzender
\newcommand\aes@afzendernaam\TAALaesnaam
\newcommand\afzendernaam[1]{\renewcommand\aes@afzendernaam{#1}}

% cienaam
\newcommand\aes@cienaam{\ClassWarningNoLine{\filename}{Geen commissienaam gedefinieerd.\MessageBreak
        Gebruik \protect\cienaam{TeXniCie}\space om het in te stellen}}
\newcommand\cienaam[1]{\renewcommand\aes@cienaam{#1}}

% KvK nummer
\def\aes@kvknr{40479641}

% btw-nummer
\def\aes@btwnr{NL816588429B01}

% mogelijke rekeningen
%BIC- en IBAN-info: http://www.ing.nl/particulier/betalen/betalen-en-het-buitenland/afkortingen-buitenlandbetalingen.aspx
\newcommand\girorekening{%
  \def\aes@bank{ING}%
  \def\aes@giro{656927}%
  \def\aes@BIC{INGBNL2A}%
  \def\aes@IBAN{NL58 INGB 0000 6569 27}%
  \def\aes@rekeninghouder{Studievereniging \aeskwadraat{} inz Fiscus}%
}
\newcommand\evenementenrekening{%
  \def\aes@bank{ING}%
  \def\aes@giro{3743305}%
  \def\aes@BIC{INGBNL2A}%
  \def\aes@IBAN{NL67 INGB 0003 7433 05}%
  \def\aes@rekeninghouder{Studievereniging \aeskwadraat{} inz Evenementen}%
}
\newcommand\ouderdagrekening{%
  \evenementenrekening%
  \ClassWarningNoLine{\filename}{Let op: De ouderdagrekening is 
opgeheven!\MessageBreak
  Waarschijnlijk wil je de evenementenrekening,\MessageBreak
  dus die heb ik maar voor je ingesteld}
}
\newcommand\introrekening{%
  \evenementenrekening%
  \ClassWarningNoLine{\filename}{Let op: De introrekening is hernoemd naar de evenementenrekening!\MessageBreak
  Waarschijnlijk wil je de evenementenrekening,\MessageBreak
  dus die heb ik maar voor je ingesteld}
}
\newcommand\boekenrekening{%
  \def\aes@bank{ABN Amro}%
  \def\aes@giro{50 7122 690}%
  \def\aes@BIC{ABNANL2A}%
  \def\aes@IBAN{NL69 ABNA 0507 1226 90}%
  \def\aes@rekeninghouder{Boekencommissaris}%
}
\girorekening

% uw kenmerk
\newcommand\aes@uwk{}
\newcommand\uwk[1]{%
  \renewcommand\aes@uwk{#1}%
}
% ons kenmerk
\newcommand\aes@onsk{}
\newcommand\onsk[1]{%
  \renewcommand\aes@onsk{#1}%
}
% e-mailadres
\newcommand\aes@email{%
        \ClassWarningNoLine{\filename}{Geen emailadres gedefinieerd.\MessageBreak
        Gebruik \protect\email{bestuur@a-eskwadraat.nl}\space om het in
        \MessageBreak te stellen}
        bestuur@a-eskwadraat.nl
}
\newcommand\email[1]{\renewcommand\aes@email{#1}}
% datum
\newcommand\aes@datum{\today}
\newcommand\datum[1]{\renewcommand\aes@datum{#1}}
% onderwerp
\newcommand\aes@subject{\ClassWarningNoLine{\filename}{Geen \protect\subject\space gevonden.\MessageBreak
        Gebruik \protect\subject{Wie dit leest is gek}\space om het in \MessageBreak
        te stellen. Standaard (leeg) subject ingevoegd}%
}
\newcommand\subject[1]{\renewcommand\aes@subject{#1}}
\let\onderwerp\subject
% bijlagen
\newenvironment{aes@blist}{
        \begin{list}{$\bullet$}{
                \raggedright
                \setlength\leftmargin{1em}
                \setlength\rightmargin{0pt}
                \setlength\labelwidth{0.6em}
                \setlength\labelsep{0.3em}
                \setlength\itemsep{0pt}
                \setlength\topsep{0pt}
                \setlength\parsep{0pt}
                \setlength\partopsep{0pt}
                \setlength\listparindent{0pt}
                \setlength\itemindent{0em}
        }
}{%
        \end{list}
}

% Deze 'counter' houdt bij hoeveel bijlage(n) gedefinieerd zijn, opdat ook dit
% briefhoofdkopje kan worden weggelaten indien leeg. Het is een commando, zodat
% hij mee doet met scoping (counters zijn altijd globaal).
\newcommand\aes@aantalbijlagen{0}
\newcommand\aes@bijlage{}
% Geen bijlagen. Equivalent aan \bijlagen{}, maar wat efficienter.
\newcommand\geenbijlagen{%
  \renewcommand\aes@bijlage{}%
  \renewcommand\aes@aantalbijlagen{0}%
}
% Wel een of meer bijlagen. Telt de bijlagen en kijkt of een lijst met bullets
% nodig is (alleen bullets bij strikt meer dan 1 bijlage).
\newsavebox{\aes@devnull}%
\newcounter{aes@telbijlagen}%
\newcommand\bijlagen[1]{%
  % Tel eerst het aantal \items. Dit doen we door \item te herdefinieren om het
  % tellertje op te hogen, en vervolgens het argument aan \bijlagen te dumpen in
  % een savebox (bij gebrek aan zwart gat).
  \let\echtitem\item%
  \setcounter{aes@telbijlagen}{0}%
  \renewcommand\item{\stepcounter{aes@telbijlagen}}%
  \savebox{\aes@devnull}{#1}%
  \let\item\echtitem%
  %
  % Nu gaan we aes@telbijlagen corrigeren en \aes@bijlage op de goede manier
  % vullen.
  \ifthenelse{\value{aes@telbijlagen}=0}{%
    \renewcommand\aes@bijlage{#1}%
    \ifthenelse{\equal{#1}{}}{%
      % Okee, echt geen bijlagen (argument is leeg).
    }{%
      % Toch 1 \item-loze bijlage.
      \setcounter{aes@telbijlagen}{1}%
    }%
  }{%
    % Wel \items geteld.
    \ifthenelse{\value{aes@telbijlagen}=1}{%
      % Slechts 1 \item. Geen bullet doen dus.
      \renewcommand\aes@bijlage{%
        \renewcommand\item{}% weg bullet
        #1
      }%
    }{%
      % Meerdere \items, dus maak een echte lijst.
      \renewcommand\aes@bijlage{%
        \begin{aes@blist}
        #1
        \end{aes@blist}
      }%
    }%
  }%
  % Sla het aantal bijlagen op het \aes@aantalbijlagen. \edef zorgt dat het
  % commando hier de huidige waarde van de counter krijgt, in plaats van dat de
  % waarde van de counter wordt opgezocht op de plek waar \aes@aantalbijlagen
  % wordt gebruikt.
  \edef\aes@aantalbijlagen{\theaes@telbijlagen}%
}
\let\bijlage\bijlagen
% \bijlages zou logischerwijs een alias moeten zijn voor \bijlagen, maar die definitie zou backwards compatibiliteit breken. Dan maar weer een warning.
\newcommand\bijlages[1]{%
  \ClassWarningNoLine{\filename}{%
  Het commando \protect\bijlages\space wordt niet meer ondersteund.\MessageBreak%
  Gebruik \protect\bijlage\space of \protect\bijlagen%
  }%
  \bijlage{#1}%
}

% kix-code
\newcommand\aes@kixcode{}
\newcommand\kixcode[1]{\renewcommand\aes@kixcode{\scantokens{#1}}}

% opening
\newcommand\opening[1]{%
  \setboolean{aes@openinggezien}{true}%
  \aes@briefhoofd%
  #1\vspace{0.5\baselineskip}\par%
}
\let\aanhef\opening
\newcommand\geenopening{%
  \setboolean{aes@openinggezien}{true}%
  \aes@briefhoofd%
}

% signature
% intern te gebruiken om 1 signature te typesetten
\newcommand\sig[1]{%
\begin{minipage}[t][2cm][t]{55mm}%
\vspace*{1.3cm}#1%
\end{minipage}
}
% intern te gebruiken voor de totale hoeveelheid signatures (1 of 2)
\newcommand\aes@signature{\ClassError{\filename}{Geen \protect\signature\space
opgegeven, maar wel een \protect\closing}
{Definieer een \protect\signature\space of haal de \protect\closing\space weg.}}

%% gebruikerscommando's
\newcommand\signature[1]{\renewcommand\aes@signature{\sig{#1}}}
\newcommand\signatures[2]{\renewcommand\aes@signature{\sig{#1}\hspace*{1cm}\sig{#2}}}
% closing
\newcommand\closing[1]{\par\vspace{0.5\baselineskip}%
% deze minipage zorgt ervoor dat de \closing en \signature altijd samen op 1 pagina staan
\begin{minipage}{\textwidth}%
#1\par\aes@signature
\end{minipage}
}

%% STANDAARD INFORMATIE, niet door gebruiker te wijzigen
\newcommand\aes@NL{\ifthenelse{\boolean{isbuitenland}}{,~NL}{}}
\newcommand\aes@returnline{\aesnaam, Princetonplein~5, 3584~CC~~Utrecht\aes@NL}
\newcommand\aes@bezoekadr{Buys Ballotgebouw, \TAALkamer~269\\
                          Princetonplein~5\\
                          3584~CC~~Utrecht\aes@NL}
\newcommand\aes@telefoon{+31\,30\,253\,44\,99}
\newcommand\aes@fax{+31\,30\,253\,57\,87}
\newcommand\aes@www{www.a-eskwadraat.nl}


% maak er een ontour stichting brief van
\ifthenelse{\boolean{aes@ontour}}{%
  \setboolean{aes@optfooter}{false}%
  \email{ontour@A-Eskwadraat.nl}%
  \afzendernaam\TAALontournaam%
  \def\aes@kvknr{30230072}%
  \def\aes@bank{ING}%
  \def\aes@giro{4559482}%
  \def\aes@BIC{INGBNL2A}%
  \def\aes@IBAN{NL39 INGB 0004 5594 82}%
  \def\aes@rekeninghouder{\TAALontournaam}%
  \renewcommand\aes@returnline{\aesnaam On Tour, Princetonplein~5, 3584~CC~~Utrecht\aes@NL}%
}{}

% maak er een betadag brief van
\ifthenelse{\boolean{aes@betadag}}{%
  \welkleur%
  \AtBeginDocument{\def\TAALfinancien#1#2{}}%
  \setboolean{aes@optfooter}{false}%
  \email{betadag@A-Eskwadraat.nl}%
  \afzendernaam{B\`etadag 2013}%
  \cienaam{Utrecht}%
  \renewcommand\aes@www{www.a-eskwadraat.nl/betadag}%
}{}

% de hoogte van een blokje in het briefhoofd
\newlength\aes@bhhoogte

% bhblok maakt een enkel blokje in het briefhoofd
% de supervieze vphantoms zorgen voor consistente hoogte
\newcommand\aes@bhblok[2]{%
        \begin{minipage}[t][\aes@bhhoogte][t]{41mm}%
                {\aes@fontheader#1\vphantom{ghL}}\par%
                {\aes@fonttext\raggedright#2\vphantom{ghL}}\par%
        \end{minipage}%
}
% shortcut voor leeg briefhoofdblokje
\newcommand\aes@leegbhblok{\aes@bhblok{~}{~}}

% Standaard briefhoofdblokjes-definitie-commando.
% Herdefinieer dit commando om zelf blokjes te plaatsen.
\newcommand\aes@briefhoofdblokjes{%
  \zetblokje{rb}{\TAALemail}{\aes@email}

  % Het bijlagenblokje hoeft alleen als er bijlagen zijn. Doet ook aan meervouddetectie.
  \ifthenelse{\aes@aantalbijlagen=1}{%
    \zetblokje{ro}{\TAALbijlage}{\aes@bijlage}
  }{%
    \ifthenelse{\aes@aantalbijlagen>1}{%
      \zetblokje{ro}{\TAALbijlagen}{\aes@bijlage}
    }{%
      \leegblokje{ro}
    }%
  }%

% geef de blokjes hun plaats, afhankelijk van de vraag of de kenmerken opgegeven zijn
  \ifthenelse{\equal{\aes@uwk}{} \and \equal{\aes@onsk}{}}{%
    \zetblokje{lb}{\TAALdatum}{\aes@datum}
    \zetblokje{mb}{\TAALonderwerp}{\aes@subject}
    \leegblokje{lo}
    \leegblokje{mo}
  }{%
    \zetblokje{lb}{\TAALuwkenmerk}{\aes@uwk}
    \zetblokje{mb}{\TAALonskenmerk}{\aes@onsk}
    \zetblokje{lo}{\TAALdatum}{\aes@datum}
    \zetblokje{mo}{\TAALonderwerp}{\aes@subject}
  }
}

% Zet een briefhoofdblokje klaar.
% #1 - Een plaatsaanduiding van twee karakters. De eerste is l (links), m
%      (midden) of r (rechts), de tweede is b (boven) of o (onder).
% #2 - Het kopje van het blokje.
% #3 - De inhoud van het blokje.
\newcommand\zetblokje[3]{%
\expandafter\gdef\csname blokje@#1\endcsname{\aes@bhblok{#2}{#3}}%
}

% Zet een briefhoofdblokje op leeg.
% #1 - Een plaatsaanduiding (zie \zetblokje).
\newcommand\leegblokje[1]{%
\expandafter\gdef\csname blokje@#1\endcsname{\aes@leegbhblok}%
}

\newlength\hoofd@temp
%% BEGIN BRIEFHOOFD
% het brief hoofd maakt -- de naam zegt het al -- de kop boven elke brief
\newcommand\aes@briefhoofd{
        \vspace*{-119mm}\par % om weer in de pas te komen met een standaard briefheader (zie \topmargin)
        \setlength\hoofd@temp{\parskip} % sla de parskip even op
        \setlength\parskip{1mm} %
        %
        % Gooi alle blokjes leeg.
        \leegblokje{lb}\leegblokje{mb}\leegblokje{rb}%
        \leegblokje{lo}\leegblokje{mo}\leegblokje{ro}%
        %
        % Laat de herdefinieerbare \aes@briefhoofdblokjes zijn gang gaan en de
        % gewenste blokjes neerzetten.
        \smash{\aes@briefhoofdblokjes}%
        %
        % eerst de bovenste 4 dingen
        \setlength\aes@bhhoogte{17mm}%
        \blokje@lb\hspace*{3mm}%
        \blokje@mb\hspace*{3mm}%
        \blokje@rb\hspace*{4mm}%
        % deze minipage is uiteraard veel hoger dan de 17mm, maar dat gaat juist goed zo
        \begin{minipage}[t][17mm][t]{45mm}%
                \raggedright
                \mbox{\aes@fontheader\bfseries\aes@afzendernaam}\par
                \mbox{\aes@fontheader\bfseries\aes@cienaam}\vspace{.9\baselineskip}\par
                {\aes@fontheader%
                \aes@bezoekadr\vspace{.9\baselineskip}\par
                \TAALtelefoon~\aes@telefoon\vspace{.9\baselineskip}\par
%               \TAALfax~\aes@fax\vspace{.9\baselineskip}\par
                \TAALfinancien{\aes@kvknr}{\aes@IBAN}\par
                \@ifundefined{\aes@btwnr}{\TAALbtw{~\aes@btwnr}\vspace{.9\baselineskip}\par}{\vspace{.9\baselineskip}\par}
                \aes@www}
        \end{minipage}\par
        %
        % dan de 3 dingen daaronder
        \setlength\aes@bhhoogte{24mm}%
        \blokje@lo\hspace*{3mm}%
        \blokje@mo\hspace*{3mm}%
        \blokje@ro\par%
        %
        % hieronder het adres e.d.
        \hspace*{5mm}% %% vanwege de vensterenvelop moet het adresveld een stukje naar rechts
        \begin{minipage}[t][47mm][t]{140mm}%
                {\aes@fontheader\aes@returnline}\\[.2ex]
                \aes@fontadres\aes@adres\\[0.5\baselineskip]
                \kix{\aes@kixcode} % dit werkt ook als \aes@kixcode leeg is; dan is er gewoon geen output
        \end{minipage}\par
        %
        % logo hieronder
        \ifthenelse{\boolean{aes@optkleur}}{}{%
        \hfill%
        \begin{minipage}[b][0cm][t]{5cm}%
                \vspace*{-35mm}\par % = -(hoogte van het logo (32mm) + 1.5mm)
                \aeslogogrijs[5cm]% logo is op het "echte" briefpapier ook 5cm breed
        \end{minipage}}
        \setlength\parskip{\hoofd@temp}
        \vspace*{4mm}\par % zorgt voor een goede afstand tot de opening
}


%% BRIEF OMGEVING
\newenvironment{brief}[1]{% het argument is het adres van de ontvanger
        \ifthenelse{\boolean{aes@optfooter}}{\thispagestyle{footer}}{} % maakt de tekst onderaan en het UU-logo
        \newcommand\aes@adres{#1}%\opening verzorgt de rest, vanwege scoping
        \newboolean{aes@openinggezien}%
        \setboolean{aes@openinggezien}{false}%
}{%
  \ifthenelse{\boolean{aes@openinggezien}}{}{%
    \ClassError{\filename}{Je bent \protect\opening{Beste Piet,}\space
    vergeten}{HekTeX wil bier.}%
  }%
        \clearpage%
}
\newenvironment{letter}[1]{\begin{brief}{#1}}{\end{brief}} % om nog een beetje op de letter-class te lijken

% en klaar!
\endinput
