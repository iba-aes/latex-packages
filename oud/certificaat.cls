% Copyright (C) 2006-2015 TeXniCie A-Eskwadraat <hektex@a-eskwadraat.nl>
%
% This file is part of the A-Eskwadraat LaTeX collection and may be
% redistributed under the terms of version 2 of the GNU General Public License
% as published by the Free Software Foundation. See LICENSE file for details.
%
%% $Id: certificaat.cls 733 2015-02-23 14:38:43Z aldo $
%
% Certificaat class voor cursus LaTeX
% gemaakt door Roland Vaandrager
%%%%




\NeedsTeXFormat{LaTeX2e}

\newcommand{\filename}{certificaat}
\newcommand{\filedate}{2006/06/05}
\newcommand{\fileversion}{0.1}

%
% TODO
% * geschikt maken voor andere cursussen
%

\ProvidesClass{\filename}[\filedate\space\fileversion\space Certificaat class voor LaTeX cursus A-Eskwadraat]


%%%% packages laden

%% we willen van alles met boxen doen, dus graphicx nodig
\RequirePackage{graphicx}
\RequirePackage{color}
\RequirePackage{ifthen}
\RequirePackage{amsmath} % voor \smash
\RequirePackage{aes}

%% is dit pdflatex?
%% papierformaat liggend A4
\ifx\pdftexversion\undefined
  \ClassError{\filename}{Deze class werkt alleen goed met pdflatex\MessageBreak Gebruik dat dus maar.}
  \newcommand{\cert@aeslogofile}{a-es2.eps}
  \newcommand{\cert@uusolfile}{uusol-links.eps}
  %\ClassError{aes2logo}{The file \cert@filelogo\space does not exist!}{}
  \setlength\paperwidth{297mm}
  \setlength\paperheight{210mm}
\else
  \newcommand{\cert@aeslogofile}{aes2-kleur.pdf}
  \newcommand{\cert@uusolfile}{uusol-links.pdf}
  \newcommand{\cert@slingerfile}{slinger.pdf}
  \setlength\pdfpagewidth{297mm}
  \setlength\pdfpageheight{210mm}
\fi

%% pagina-stijl, marges e.d.

\setlength\hoffset{-1in}
\setlength\voffset{-1in}
\setlength\headheight{0pt}
\setlength\headsep{0pt}
\setlength\oddsidemargin{25mm}
\setlength\evensidemargin{25mm}
\setlength\topmargin{15mm}
\setlength\textwidth{247mm}
\setlength\textheight{180mm}

%% font-meuk

\renewcommand{\encodingdefault}{OT1}
\renewcommand{\familydefault}{\rmdefault}
\renewcommand{\seriesdefault}{m}
\renewcommand{\shapedefault}{n}
\renewcommand{\rmdefault}{pnc}  %% default serif font: New Century Schoolbook
\renewcommand{\sfdefault}{cmss} %% default sans-serif: New Century Sans-Serif
\renewcommand{\ttdefault}{cmtt} %% default teletype  : New Century Typewriter

\renewcommand{\normalsize}{\fontsize{17.28pt}{18pt}\selectfont}

% speciale font-definities
\newcommand{\cert@kopfont}{\bfseries}
\newcommand{\cert@deelnemerfont}{\fontsize{20.74pt}{22pt}\selectfont\bfseries}

%%% \today heel hard gekaapt uit article.cls en aangepast
\newcommand{\today}{\number\day\space\ifcase\month\or
  januari\or februari\or maart\or april\or mei\or juni\or
  juli\or augustus\or september\or oktober\or november\or december\fi
  \space\number\year}

%%% standaard-commando's

\let\AEss\aesnaam

\setlength\parindent{0pt}
\definecolor{AchtergrondGrijs}{rgb}{0.9,0.9,0.9}
\definecolor{AEssrood}{rgb}{0.59,0,0.12}

% logo's
\newcommand{\cert@uusol}{\includegraphics[width=8cm]{\cert@uusolfile}}
\newcommand{\cert@aeslogo}{\includegraphics[width=6cm]{\cert@aeslogofile}}
%%% de standaardwaarden van variabelen

%% naam deelnemer
\newcommand{\cert@deelnemer}{\ClassWarningNoLine{\filename}{Geen deelnemer opgegeven.\MessageBreak
Dat lijkt me toch vrij essentieel\MessageBreak voor dit certificaat}
Deelnemer}
%% naam voorzitter
\newcommand{\cert@voorzitternaam}{\ClassWarningNoLine{\filename}{Geen naam voor de voorzitter opgegeven.\MessageBreak
ingevoegd: ``Jan Jitse Venselaar''}
Jan Jitse Venselaar}
%% datum cursus
\newcommand{\cert@cursusdatumvan}{\ClassWarningNoLine{\filename}{Geen cursusdatum opgegeven.\MessageBreak
Ik ga uit van de datum van vandaag}
\today}
% de cursus heeft standaard maar 1 datum, cursusdatumtot geeft een range aan
\newcommand{\cert@cursusdatumtot}{}
%% datum uitgave
\newcommand{\cert@datum}{\ClassWarningNoLine{\filename}{Geen datum van uitgave opgegeven.\MessageBreak
Ik ga uit van de datum van vandaag}
\today}
%% cursusnaam
\newcommand{\cert@cursusnaam}{\ClassWarningNoLine{\filename}{Geen cursusnaam opgegeven.\MessageBreak
Dat lijkt me toch vrij essentieel\MessageBreak voor dit certificaat.\MessageBreak
Standaardwaarde ingevoegd}
\LaTeX{} voor beginners}
\newcommand\cert@stdafgerond{met goed gevolg heeft afgerond.}
\newcommand{\cert@afgerond}{\cert@stdafgerond}

%%% deze commando's zijn ervoor om belangrijke gegevens te herdefinieren
\newcommand{\deelnemer}[1]{\renewcommand{\cert@deelnemer}{#1}}
\newcommand{\cursusdatum}[1]{\renewcommand{\cert@cursusdatumvan}{#1}}
\newcommand{\cursusdatumvan}[1]{\cursusdatum{#1}}
\newcommand{\cursusdatumtot}[1]{\renewcommand{\cert@cursusdatumtot}{#1}}
\newcommand{\datum}[1]{\renewcommand{\cert@datum}{#1}}
\newcommand{\cursusnaam}[1]{\renewcommand{\cert@cursusnaam}{#1}}
\newcommand{\voorzitternaam}[1]{\renewcommand{\cert@voorzitternaam}{#1}}
\newcommand{\cumlaude}{\renewcommand{\cert@afgerond}{heeft afgerond met judicium \textcolor{red}{Cum Laude}}}

%%% hier staan de commando's die de lay-out maken
\newcommand{\cert@kop}{\begin{center}
\resizebox{0.35\textwidth}{!}{\cert@kopfont Certificaat}
\end{center}}

\newcommand{\cert@handtekening}[3][\cert@datum]{
\begin{minipage}{8cm}
\makebox[8cm]{\hrulefill}
#2,\\
#3\\[1ex]
#1
\end{minipage}
}

\newcommand{\cert@zethandtekeningen}{
\cert@handtekening{\cert@voorzitternaam}{voorzitter~\TeX niCie}\hfill\cert@handtekening{\cert@deelnemer}{deelnemer}
}

\newcommand{\cert@tekst}{\cert@kop\vspace*{1cm}
De \TeXniCie{} verklaart dat%\hfill Utrecht, {\cert@deelnemerfont\cert@datum}
\\[3ex]
\hspace*{2em}\smash{\cert@deelnemerfont\cert@deelnemer}\\[2ex]
de cursus%\\[3ex]
\hspace{1em}
{\cert@deelnemerfont\cert@cursusnaam}\hspace{1em}van \AEss\\[2ex]
%, die \ifthenelse{\equal{\cert@cursusdatumtot}{}}{% de datum is vast
%op\\[1.5ex]
%\hspace*{2em}{\cert@deelnemerfont\cert@cursusdatumvan}\\[1.5ex]
%}{% de datum is een range
%van\\[1.5ex]
%\hspace*{2em}{\cert@deelnemerfont\cert@cursusdatumvan}\hspace{1em}tot\hspace{1em}{\cert@deelnemerfont\cert@cursusdatumtot}\\[1.5ex]
%}
%is gegeven,
\cert@afgerond
\vfill
\cert@zethandtekeningen
}

\newcommand{\cert@plaatslogos}{\begin{minipage}{\textwidth}
\begin{minipage}[c]{8cm}
\cert@uusol
\end{minipage}
\hfill
\begin{minipage}[c]{6cm}
\cert@aeslogo
\end{minipage}
\end{minipage}
}

\newcommand{\cert@achtergrond}{
\setlength\unitlength{1mm}
\begin{picture}(0,0)(15,-10)
% rode lijntjes als kader
\linethickness{1mm}
\put(0,0){\color{AEssrood}{\line(1,0){277}}}
\put(0,-190.5){\color{AEssrood}{\line(0,1){191}}}
\put(0,-190){\color{AEssrood}{\line(1,0){277}}}
\put(277,-190.5){\color{AEssrood}{\line(0,1){191}}}
\put(10,-160){\includegraphics[width=\textwidth]{\cert@slingerfile}}

\end{picture}
}

\newcommand\maakcertificaat{\cert@achtergrond%
                            \cert@tekst%
                            \cert@plaatslogos%
                            \clearpage%
                            \renewcommand\cert@afgerond\cert@stdafgerond%
                            }
\endinput
