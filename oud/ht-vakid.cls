% Copyright (C) 2002-2015 TeXniCie A-Eskwadraat <hektex@a-eskwadraat.nl>
%
% This file is part of the A-Eskwadraat LaTeX collection and may be
% redistributed under the terms of version 2 of the GNU General Public License
% as published by the Free Software Foundation. See LICENSE file for details.
%
%% $Id: ht-vakid.cls 733 2015-02-23 14:38:43Z aldo $
%
%% Vakidioot class, oorspronkelijk gemaakt door Henk Bearda en Theo van den Bogaart 2002
%% Aangepast door Jacob Kleerekoper en Jan Jitse Venselaar

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{ht-vakid}[2005/04/21 v4.04 The Vakidioot Class]
\LoadClass[twoside,12pt]{article}
\RequirePackage[utf8]{inputenc}

\special{header=dubbelzijdig}   %moest van Bas

%initialisatie
\ProcessOptions
\special{"
	/cm {28.35 mul} def % definieert cm's 1 inch = 2.54
	/Lichtgrijs {0.85} def
	}
%Een aantal definities die in Postscript-specials worden gebruikt.
\edef\@lichtgrijs{0.90}		%definieert de kleur van de idiootzijbalken
\edef\@donkergrijs{0.80}	%definieert de kleur van de vakzijbalken


%\RequirePackage[dutch,english,french,german]{babel}
%\RequirePackage{chess,graphicx,multicol,eurosym,wrapfig,verbatim,amssymb,amsmath,amsfonts,amsthm,boxedminipage,marvosym}
\RequirePackage{graphicx,multicol,eurosym,wrapfig,boxedminipage}
\RequirePackage[usenames]{color}

% Advertorial-omgeving
\RequirePackage{advertorial}

%bladspiegel
%
\setlength{\paperwidth}{215mm}
\setlength{\textwidth}{0.70\paperwidth}
\setlength{\oddsidemargin}{0.15\paperwidth}
\setlength{\evensidemargin}{0.15\paperwidth}
\setlength{\hoffset}{-1in}
\setlength{\marginparsep}{0mm}
\setlength{\marginparwidth}{0mm}
%
\setlength{\paperheight}{297mm}
\setlength{\textheight}{0.8\paperheight}
\setlength{\voffset}{-1in}
\setlength{\topmargin}{0.06\paperheight}
\setlength{\headheight}{0.03\paperheight}
\setlength{\headsep}{0.03\paperheight}
\setlength{\footskip}{0mm}
\setlength{\unitlength}{1mm}

% headings
%
%Dit is de oneven versie de grijze randen rond idioot artikelen
%Jan Jitse
\newcommand{\ps@idioot}{%
	\global\def\@oddfoot{}
	\global\def\@evenfoot{\@oddfoot}
	\global\def\@oddhead{%
	  \special{"
	  	/lg {\@lichtgrijs} def
		/cm {28.35 mul} def % 72/2.54
		gsave lg setgray
		newpath
			-20 0 moveto
			-15 20 rlineto
			515 0 rlineto
			0 -20 rlineto
		closepath fill
		newpath
			-30 0 moveto
			-15 20 rlineto
			7 0 rlineto
			15 -20 rlineto
		closepath fill
		newpath
			-38 0 moveto
			-15 20 rlineto
			5 0 rlineto
			15 -20 rlineto
		closepath fill
		newpath
			-44 0 moveto
			-15 20 rlineto
			3 0 rlineto
			15 -20 rlineto
		closepath fill
		newpath
			479 -5 moveto
			-18 0 rlineto
			0 -118 rlineto
			18 -15 rlineto
		closepath 2 setlinewidth stroke
		newpath
			480 -145 moveto
			-20 16 rlineto
			0 -180 rlineto
			20 -16 rlineto
		closepath fill
		newpath
			480 -501 moveto
			-20 16 rlineto
			0 -65 rlineto
			20 -16 rlineto
		closepath fill
		1 setlinewidth 0 setgray grestore}%
%Dit is de even versie voor idioot boven de idioot artikelen
%Jan Jitse
	  \color{white}%
	  \hbox to0pt{\hbox to460pt{~\hss}\vbox to20pt{\vfil%
		\hbox to0pt{\hss%
		{\fontfamily{pbb}\fontseries{b}\fontshape{sl}\selectfont\Large%
		   Idioot}}%
	  \vfil}\hss}%
%		\vaklogo{0.25}{blue}%}%
	  \hbox to0pt{\hbox to471pt{\hfil%
	    \vbox to0pt{\vskip18.37cm%
		\hbox to0pt{\hss%
	 	    {\fontfamily{phv}\fontseries{b}\selectfont\large\thepage}%
		\hss}%
	    \vss}%
	  }\hss}%
	  \color{black}\hfil}
	\global\def\@evenhead{%
	  \hfil%
	  \special{"
		/lg {\@lichtgrijs} def
		/cm {28.35 mul} def % 72/2.54
		gsave lg setgray
		%dit is de grote grijze balk bovenaan
		newpath
			35 0 moveto
			-15 20 rlineto
			-500 0 rlineto
			0 -20 rlineto
		closepath fill
		% dit is de eerste scheve balk bovenaan
		newpath
			45 0 moveto
			-15 20 rlineto
			-7 0 rlineto
			15 -20 rlineto
		closepath fill
		% dit is de tweede scheve balk bovenaan
		newpath
			53 0 moveto
			-15 20 rlineto
			-5 0 rlineto
			15 -20 rlineto
		closepath fill
		%dit is de derde scheve balk bovenaan
		newpath
			59 0 moveto
			-15 20 rlineto
			-3 0 rlineto
			15 -20 rlineto
		closepath fill
		%Dit is de lege balk aan de zijkant
		newpath
			-479 -5 moveto
			18 0 rlineto
			0 -133 rlineto
			-18 15 rlineto
		closepath
		2 setlinewidth stroke % Dit bepaalt de dikte van de lege balk
		%Dit is de dichte balk aan de zijkant
		newpath
			-480 -130 moveto
			20 -16 rlineto
			0 -180 rlineto
			-20 16 rlineto
		closepath fill
		%Dit is de balk met de inhoudsopgave
		newpath
			-480 -486 moveto
			20 -16 rlineto
			0 -65 rlineto
			-20 16 rlineto
		closepath fill
		1 setlinewidth 0 setgray
		grestore}%

%Dit is de oneven versie voor idioot boven de idioot artikelen
%Jan Jitse
	    \color{white}%
	    \hbox to0pt{\hss\vbox to20pt{\vfil%
	      \hbox to0pt{%
		{\fontfamily{pbb}\fontseries{b}\fontshape{sl}\selectfont\Large%
		  Idioot}\hss}%
	      \vfil}\hbox to460pt{~\hss}}%
	    \hbox to0pt{\hss\hbox to472pt{%
	      \vbox to0pt{\vskip18.37cm%
		\hbox to0pt{\hss%
	 	    {\fontfamily{phv}\fontseries{b}\selectfont\large\thepage}%
	  	\hss}%
	      \vss}%
	    \hfil}}%
	  \color{black}}%
}

\newcommand{\ps@vak}{%
	\global\def\@oddfoot{}
	\global\def\@evenfoot{\@oddfoot}
	\global\def\@oddhead{%
	  \special{"
		/dg {\@donkergrijs} def
		/cm {28.35 mul} def % 72/2.54
		gsave dg setgray
		newpath
			-20 0 moveto
			-15 20 rlineto
			515 0 rlineto
			0 -20 rlineto
		closepath fill
		newpath
			-30 0 moveto
			-15 20 rlineto
			7 0 rlineto
			15 -20 rlineto
		closepath fill
		newpath
			-38 0 moveto
			-15 20 rlineto
			5 0 rlineto
			15 -20 rlineto
		closepath fill
		newpath
			-44 0 moveto
			-15 20 rlineto
			3 0 rlineto
			15 -20 rlineto
		closepath fill
		newpath
			479 -5 moveto
			-18 0 rlineto
			0 -118 rlineto
			18 -15 rlineto
		closepath
		2 setlinewidth stroke
		newpath
			480 -145 moveto
			-20 16 rlineto
			0 -180 rlineto
			20 -16 rlineto
		closepath fill
		newpath
			480 -501 moveto
			-20 16 rlineto
			0 -65 rlineto
			20 -16 rlineto
		closepath fill
		1 setlinewidth 0 setgray grestore}%

%Dit is de oneven versie voor boven vak-artikelen
%Jan Jitse

	  \color{white}%
	  \hbox to0pt{\hbox to460pt{~\hss}\vbox to20pt{\vfil%
		\hbox to0pt{\hss%
		{\fontfamily{pbb}\fontseries{b}\fontshape{sl}\selectfont\Large%
		   Vak}}%
	  \vfil}\hss}%
	  \hbox to0pt{\hbox to471pt{\hfil%
	    \vbox to0pt{\vskip18.37cm%
		\hbox to0pt{\hss%
	 	    {\fontfamily{phv}\fontseries{b}\selectfont\large\thepage}%
		\hss}%
	    \vss}%
	  }\hss}%
	  \color{black}\hfil}
	\global\def\@evenhead{%
	  \hfil%
	  \special{"
		/dg {\@donkergrijs} def
		/cm {28.35 mul} def % 72/2.54
		gsave dg setgray
		newpath
			35 0 moveto
			-15 20 rlineto
			-500 0 rlineto
			0 -20 rlineto
		closepath fill
		newpath
			45 0 moveto
			-15 20 rlineto
			-7 0 rlineto
			15 -20 rlineto
		closepath fill
		newpath
			53 0 moveto
			-15 20 rlineto
			-5 0 rlineto
			15 -20 rlineto
		closepath fill
		newpath
			59 0 moveto
			-15 20 rlineto
			-3 0 rlineto
			15 -20 rlineto
		closepath fill
		newpath
			-479 -5 moveto
			18 0 rlineto
			0 -133 rlineto
			-18 15 rlineto
		closepath
		2 setlinewidth stroke
		newpath
			-480 -130 moveto
			20 -16 rlineto
			0 -180 rlineto
			-20 16 rlineto
		closepath fill
		newpath
			-480 -486 moveto
			20 -16 rlineto
			0 -65 rlineto
			-20 16
		rlineto closepath fill
		1 setlinewidth 0 setgray grestore}%

%Dit is de even versie voor boven vak-artikelen
%Jan Jitse
	    \color{white}%
	    \hbox to0pt{\hss\vbox to20pt{\vfil%
	      \hbox to0pt{%
		{\fontfamily{pbb}\fontseries{b}\fontshape{sl}\selectfont\Large%
		   Vak}\hss}%
	      \vfil}\hbox to460pt{~\hss}}%
	    \hbox to0pt{\hss\hbox to472pt{%
	      \vbox to0pt{\vskip18.37cm%
		\hbox to0pt{\hss%
	 	    {\fontfamily{phv}\fontseries{b}\selectfont\large\thepage}%
	  	\hss}%
	      \vss}%
	    \hfil}}%
	  \color{black}}%
}

\renewcommand{\ps@empty}{%
	\global\def\@oddfoot{}
	\global\def\@evenfoot{\@oddfoot}
	\global\def\@oddhead{}
	\global\def\@evenhead{\@oddhead}
}
\pagestyle{idioot}



% tekst-eigenschappen
%
\setlength{\parindent}{0mm}
\setlength{\parskip}{0pt}



%theo-stuff
\newenvironment{legepagina}
  {\setcounter{figure}{0}%
  \clearpage\thispagestyle{empty}\vbox
  to.99\vsize\bgroup\vfil}{\vfil\egroup}

\newskip\@kopje
\newskip\RuimteTussenKopEnAuteur
\newskip\RuimteTussenKopEnSubkop
\RuimteTussenKopEnAuteur14pt plus4pt minus1pt
\RuimteTussenKopEnSubkop2pt plus1pt

\newif\ifniet@ininhoud
\def\vakartikel{\@ifnextchar*{\niet@ininhoudtrue\@vakartikel}
  {\niet@ininhoudfalse\@vakartikel*}}
\def\idiootartikel{\@ifnextchar*{\niet@ininhoudtrue\@idiootartikel}
  {\niet@ininhoudfalse\@idiootartikel*}}
\def\@vakartikel*#1{\@ifnextchar[{\@@vakartikel{#1}}{\@@vakartikel{#1}[]}}
\def\endvakartikel{\end@@vakartikel}
\def\@idiootartikel*#1{\@ifnextchar[{\@@idiootartikel{#1}}{\@@idiootartikel{#1}[]}}
\def\endidiootartikel{\end@@idiootartikel}
\def\endcartoon{\end@@cartoon}
\def\subartikel{\@ifnextchar*{\niet@ininhoudtrue\@subartikel}
  {\niet@ininhoudfalse\@subartikel*}}
\def\@subartikel*#1{\@ifnextchar[{\@@subartikel{#1}}{\@@subartikel{#1}[]}}
\def\endsubartikel{\end@@subartikel}

\def\@@vakartikel#1[#2]#3#4{% 1: Titel 2: Subtitel 3: Auteur 4:plaatje
  \clearpage
  \pagestyle{vak}
  \@@subartikel{#1}[#2]{#3}
  \vskip\RuimteTussenKopEnAuteur
  \vbox{\rightskip0pt plus3cm \fontfamily{phv}\selectfont#3}
  \ifniet@ininhoud\else
  \addtocontents{toc}{\protect\vakcontentsline{#1}{#3}{\thepage}}\fi
 % \nointerlineskip
  \vskip\baselineskip
  \nobreak
  \def\temp@{#4}
  \ifx\temp@\empty
    \def\@lead##1\par{{\bfseries##1}\begin{multicols}{2}}
  \else
    \setbox1\hbox to\textwidth{\hfil
      \includegraphics[width=.3\textwidth]{#4}}
    \dimen1\ht1
    \vbox to0pt{\null\box1\vss}
    \def\@lead##1\par{
      \setbox1\vbox{
        \bfseries
        \dimen2\dimen1
        \advance\dimen2 by\baselineskip
        \divide\dimen2 by\baselineskip
        \hangafter-\dimen2
        \hangindent-.32\textwidth
        ##1}
      \ifdim\ht1<\dimen1
        \vbox to\dimen1{\vfil\box1}
      \else
        \box1
      \fi
      \begin{multicols}{2}
      }
  \fi
  \@lead}
\def\end@@vakartikel{\end{multicols}\end{nochess}}

\def\@@subartikel#1[#2]#3{%1: titel 2: subtitel 3: auteur
  \def\@auteur{#3}
  \selectlanguage{dutch}
  \setcounter{figure}{0}
  \vbox{\begin{flushleft}%
    \fontfamily{phv}\selectfont\huge#1\par
    \def\t@st{#2}\ifx\t@st\empty\else
      \vskip\RuimteTussenKopEnSubkop
      \vbox{\rightskip0pt plus3cm \fontfamily{phv}\selectfont\Large#2}
    \fi\end{flushleft}}
  \nobreak
  \def\kopje{%
    \@startsection{subsection}{2}{\z@}{-1.5\bigskipamount}{\smallskipamount}%
    {\fontshape{sl}\fontfamily{phv}\selectfont\large}*}
  \def\vraag##1{\textit{##1}\\}
  \def\antwoord##1{##1\\}
  \begin{nochess}% Pas hier en niet eerder, anders zijn
                 % \kopje e.d. niet gedefinieerd buiten de nochess
  \expandafter\textbf\bgroup
  \def\par{\egroup\vskip\baselineskip
    \parskip0pt plus1pt \relax}\ignorespaces
}

\def\@@idiootartikel#1[#2]#3{%
  \clearpage
  \pagestyle{idioot}
  \ifniet@ininhoud\else
  \addtocontents{toc}{\protect\idiootcontentsline{#1}{#3}{\thepage}}\fi
  \@@subartikel{#1}[#2]{#3}
}

\def\cartoon#1#2{%
  \def\@auteur{#2}
  \clearpage
  \selectlanguage{dutch}
  \setcounter{figure}{0}
  \pagestyle{idioot}
  \addtocontents{toc}{\protect\idiootcontentsline{#1}{#2}{\thepage}}
  \nobreak
  \expandafter\textbf\bgroup
  \def\par{\egroup\vskip\baselineskip
    \parskip0pt plus1pt \relax}\ignorespaces
}

\def\end@@idiootartikel{
 \end@@subartikel
}

\def\end@@cartoon{
  \vskip\baselineskip
}

\def\end@@subartikel{%
\vskip\baselineskip
  \end{nochess}
  {\leftskip=0pt plus1fil
   \rightskip=0pt plus1fil
 \fontfamily{phv}\selectfont\parfillskip0pt\let\\\par
 \@auteur\par}
}

\long\def\inhoud#1{
  \clearpage
  \thispagestyle{empty}
  \bgroup
  \def\@hakkopop##1{%
    \def\@kop{}%
    \def\@test####1\\{%
      \ifx\relax####1\else\edef\@kop{\@kop ####1}\expandafter\@test\fi}%
    \@test##1\\\relax\\%
    }
  \vbox to\textheight{
    \hbox to\textwidth{\hfil\fontfamily{phv}\selectfont\huge In dit nummer\hfil}
    \vskip3\baselineskip
    \hbox{\fontfamily{phv}\fontsize{20}{24}\selectfont Vakartikelen}
    \def\vakcontentsline##1##2##3{
      \vskip12pt minus12pt
      \hbox to.5\textwidth{%
        \vbox{
          \hsize.5\textwidth
          \rightskip.1\textwidth plus0.25\textwidth
          \@hakkopop{##1}
          \parfillskip0pt
          \strut\@kop%
          \leaders\hbox to1em{\hss.\hss}\hfil##3\strut%
          \hskip-.1\textwidth\null%
          }%
        }
      \vskip-5pt
      {\def\\{}  %<-voorkomt dat \edef stukexpandeert
      \edef\t@st{##2}\edef\t@@st{}\ifx\t@st\t@@st\vskip2pt\else
        \vbox{\let\\\par\null\itshape\strut##2\strut}
      \fi}
      }
    \def\idiootcontentsline{
      \hbox to\textwidth{\hfil\fontfamily{phv}\fontsize{20}{24}\selectfont
        Idioot-artikelen}
      \def\idiootcontentsline####1####2####3{
        \vskip12pt minus12pt
        \hbox to\textwidth{%
          \hbox to.5\textwidth{\hfil####3}\hskip0pt plus0.00001pt
					   %^^^^afrondfoutje
          \@hakkopop{####1}%
          \vtop{\hsize.5\textwidth
            \leftskip.1\textwidth plus.25\textwidth
            \parfillskip0pt
            \hangafter-1 \parindent-.1\textwidth
            \strut
            \leaders\hbox to1em{\hss.\hss}\hskip.1\textwidth plus1fill
            \relax
            \@kop\strut
            }%
          }
        \vskip-5pt
        }
      \idiootcontentsline}
    \@starttoc{toc}
    \vskip8pt plus1fil
    \hrule
    \vskip.25\baselineskip
    \vbox{\null#1}
    }
  \egroup
  }

\newenvironment{redactioneel}[4]	%aanroep:
					%\begin{redactioneel}
					%  {<uitgavedatum>}
					%  {<oplage>}
					%  {<deadline volgend nummer>}
					%  {<dankpers. 1>\\<...>}
{
  \def\@hoofdredacteur{%
  \hoofdredacteur\\
  Hoofdredacteur}
  \clearpage
  \selectlanguage{dutch}
  \thispagestyle{empty}
  \vbox to0pt{
    \hbox to.35\textwidth{%
      \vbox to\textheight{
        \null
        \hsize.34\textwidth
        \def\\{\par\ignorespaces}
        {\huge\strut\large Colofon}
        \vskip\RuimteTussenKopEnAuteur
        \vbox{\strut}
        \fontsize{9pt}{11pt}\selectfont
        \parskip.5\baselineskip
        \textit{datum uitgave:} #1\par
        \textit{oplage:} #2\par
        \textit{deadline volgend nummer:}\\#3\par
        \vfil
        De Vakidioot \textit{is een uitgave van:}\\
        Studievereniging A--Eskwadraat\\
        Princetonplein 5\\
        3584 CC~~Utrecht\\
        \textit{tel:} (030)\kern1pt 253\kern1pt 4499\\
        \textit{fax:} (030)\kern1pt 253\kern1pt 5787\\
        \textit{e-mail:} vakidioot@a-eskwadraat.nl\par
        \vfil
        \textit{redactie:}\\
		\redactie\par
        \vfil
        \textit{Met dank aan:}\\
        #4\par
      }
      \hss
      \vrule width 1pt height\textheight depth0pt
    }
    \vss
  }
  \leftskip.37\textwidth
  \textwidth.53\hsize
  \hbox to\hsize{\hskip.37\hsize\hfil
    \huge\textbf{R}edactioneel\hfil}
  {\dimen0\RuimteTussenKopEnAuteur
  \vskip\dimen0 } %niet perse nodig (we gebruiken al natural lengths...)
  \vbox{\strut}
  \setbox1\hbox{\includegraphics[height=5.5\baselineskip]{hoofdredacteur}}
  \renewenvironment{quotation}{}{}
  \dimen1\wd1
  \advance\dimen1 by.5em
  \vbox to0pt{\null\hbox to\hsize{\hfil\box1}\vss}
  \hangafter-6 \hangindent-\dimen1
  \bgroup\def\par{\hskip\parfillskip\penalty-500}  %een vuil hekje,
						   %maar redactioneel
						   %is maar 1 pagina...
}{
  \egroup
  \vskip2\baselineskip
  \@hoofdredacteur\par
}



%gebruik:
%\achtergrondfoto{wouter.eps}
%\fotovak[6cm,3cm]{10cm}{foto.eps} %verschuivng[,] optioneel
                                  % 10cm is breedte
%\jaar{2002}  %optioneel
%\nummer{3}
%\titel{Verleden en Toekomst}
%\maakvoorkant
\def\achtergrond#1#2#3#4{\def\@achterfoto{%
  \includegraphics[height=1.05\paperheight,width=1.05\paperwidth]{#1}} % dit zou de achtergrondfoto 1.05 x zo groot moeten maken als de papierspiegel, maar Latex laat niks groter dan de papierrand toe?
  \definecolor{@@randkleur}{rgb}{#2,#3,#4}
  \edef\@randkleur{#2 #3 #4 } % definieert de achtergrondkleur, als functie van de 2e, 3e en 4e meegegeven optie met achtergrond
}
\def\@aeslogo{%
  \setbox0\vbox{\vskip23.5cm
    \includegraphics[width=2.6cm]{aes2logo-grijs.eps}} \ht0=0pt \dp0=0pt
  \moveright14.3cm %zorgt voor de juiste positionering
  \box0 %zorgt ervoor dat het logo op de pagina (box0) komt
  }
\def\jaar#1/#2#3{\def\@jaar{'#1--'#2#3}}
\def\nummer#1{\def\@nummer{#1}}
\def\titel#1{\def\@titel{#1}}
\def\maakvoorkant{
	\begin{titlepage}
	\offinterlineskip
	\null % doet niets?
	\setbox0\vbox{\vskip-5.2cm
  		\hsize=\maxdimen\@achterfoto}
	\wd0=0pt \ht0=0pt \dp0=0pt %definieert de box waar de achtergrond in komt?
	\moveleft5cm % zorgt voor de juiste positionering van de achtergrond?
	\box0
	\box0
	\@aeslogo
	\ht0=0pt \dp0=0pt
	\box0

	\setbox0\vbox{\special{"
		gsave
		/cm {28.45 mul} def 	% 72/2.54
		/u {4 mul} def 		% Waarom met 4 vermenigvuldigen?
		\@randkleur setrgbcolor	% Bepaalt de kleur
		%% Bovenste balk
		newpath
			17.00 cm 0 moveto % verschuiving naar rechts
			0 20 u rlineto % verschuiving naar boven
			-22 cm 0 rlineto % verschuiving naar links
			0 -20 u rlineto % verschuiving naar beneden
		closepath fill
		% Bovenste zijbalk
		newpath
			17.00 cm -2 u add -5 u moveto % verschuiving naar rechts
			-16 u 0 u rlineto
			0 u -118 u rlineto
			64 -320 6 div rlineto
			%16 u -15 18 div 16 mul u rlineto % eh? waarom niet 64 -53.3 rlineto?
		closepath
		gsave
			4 u setlinewidth stroke % zorgt voor de rode band aan de buitenkant van de rechterbalk
		grestore
		1 setgray fill % vult de rechterbalk met wit.
		\@randkleur setrgbcolor
		% Onderste zijbalk
		newpath
			17.00 cm -145 u moveto % verschuiving naar rechts
			-20 u 16 u rlineto
			0 u -11 cm rlineto
			20 u 0 rlineto
		closepath fill
		grestore
	}}
	\ht0=0pt \wd0=0pt \dp0=0pt
	\box0
		%\@fotovak % Niet meer in gebruik
	\@aeslogo
%Dit definieert de informatie op de voorkant. Hierin moet ergens de witte achtergrond voor de letters zitten.
\setbox0\vbox{\hsize.5\textwidth
  \fontfamily{phv}\fontseries{bx}\fontsize{14pt}{0pt}\selectfont
  \setbox1\hbox{Studievereniging A--Eskwadraat}\copy1
  \vskip10pt
  \hbox to\wd1{Jaargang \@jaar\hfil nummer \@nummer }
  } \ht0=0pt \dp0=0pt

\vskip-2cm
\moveright8.61cm\box0
\vskip2cm

%Dit maakt de titel op de voorkant
\setbox0\vbox{
  	\special{ps:
    		gsave
    		270 rotate}
  	\hbox to0pt{%
    		\fontfamily{phv}\fontseries{bx}\fontsize{35pt}{0pt}\selectfont
    		\@titel\hss}
  	\special{ps: grestore
  	}} \ht0=0pt \dp0=0pt
\vskip14.4cm
\moveleft19.6cm\box0
\vskip-14.4cm

%Dit maakt het Vakidlogo op de voorpagina
\setbox0\vbox{
  	\special{"
  	gsave % Gooit de huidige graphics state op de stack
  	/cm {28.45 mul} def
  	2.20 cm 0 cm translate % verplaatst het vak-gedeelte van het logo naar de goede plek
  	0.9 0.9 scale  %Schaalt het vak gedeelte van het logo op
  	/skstartmatrix matrix currentmatrix def
  	/tmpmat matrix def
  	/T {
		/tmpmat tmpmat currentmatrix def 	% definitie in een definitie? Vage shit...
		dup type				% En was trouwens al gedefinieerd :?
    		/arraytype eq
			{concat}
			{translate}
		ifelse
		0 0 moveto
    		tmpmat exch show popt} def
  	/pusht {matrix currentmatrix} bind def
  	/popt {setmatrix} bind def
  	0 0 0 setrgbcolor % Zinloos? Kleur is al standaard zwart?
	/Helvetica findfont 72 scalefont setfont (vak)
  	[1.12742 0 0 1 60.6181 -7 cm add 4.70 1 cm add]
	%[1.12742 0 0 1 60.6181 -7 cm add 4.70 1 cm add] %586.859] % schaling van de fonts
	%1: breedte van het font
	%2: angle van de lijn
	%3: schuinheid letters (italic)
	%4: hoogte van de fonts
	%5: horizontale positie
	%6: verticale positie
  	T
  	/AvantGarde-BookOblique findfont 72 scalefont setfont (idioot)
  	[0.592643 0.00132991 0.131933 -0.449422 75.2941 -7 cm add 1 cm]
	%[0.592643 0.00132991 0.131933 -0.449422 75.2941 -7 cm add 1 cm] %582.161] T % schaling van de fonts
	T
  	%Dit maakt de lijn tussen Vak en Idioot in het vakid-log
  	newpath
  		63.5809 -7 cm add  2.27 1 cm add moveto %584.435
  		202.291 -7 cm add  2.27 1 cm add lineto %idem
  	1 setlinewidth
  	0 setlinejoin
  	0 setlinecap
  	[] 0 setdash
  	pusht
  	skstartmatrix
  	setmatrix
  	stroke %Tekent de lijn daadwerkelijk
  popt
  grestore % Herstelt de graphics state naar de laatste op de stack
  }}
\box0
\end{titlepage}
}

\def\figuur{\@ifnextchar*{\@@figuur}{\@figuur}}
\def\@figuur#1{\nobreak\refstepcounter{figure}{\scriptsize\textbf{Figuur \thefigure:~} }{\scriptsize#1}}
\def\@@figuur#1#2{\nobreak{\scriptsize#2}}


\endinput
