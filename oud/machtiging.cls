% Copyright (C) 2009-2014 TeXniCie A-Eskwadraat <hektex@a-eskwadraat.nl>
%
%
%% $Id: machtiging.cls 391 2009-03-13 19:19:09Z roland $
%% A-Eskwadraat machtiging class
%
\NeedsTeXFormat{LaTeX2e}[1999/06/01]

%% BESTANDSINFORMATIE
\newcommand\filename{machtiging}
\newcommand\filedate{2009/06/10}
\newcommand\fileversion{0.2}

\ProvidesClass{\filename}[\filedate\space v\fileversion\space A-Eskwadraat machtigingskaart]

\ClassError{\filename}{%
  De machtiging-class wordt niet meer onderhouden.\MessageBreak
  Gebruik, in plaats van deze class, de aesbrief-class\MessageBreak
  in combinatie met de machtiging-package%
}{Neem voor meer hulp contact op met de TeXniCie.}


%% ARTICLE LADEN
\LoadClass{article}

%% PAGINA LAY-OUT
\setlength\paperwidth{210mm}
\setlength\paperheight{297mm}
\setlength\pdfpagewidth{210mm}
\setlength\pdfpageheight{297mm}
%
\setlength\hoffset{-1in}
\setlength\voffset{-1in}
%
\setlength\oddsidemargin{21mm}
\setlength\evensidemargin{21mm}
\setlength\topmargin{117mm} % 117mm is noodzakelijk voor een eventuele tweede pagina (zie ook \aes@briefhoofd)
                            % hiermee komt de tekst van pagina 2 precies op de hoogte van de tekst van pagina 1
\setlength\headheight{0mm}
\setlength\headsep{0mm}
%
\setlength\parindent{0pt}
\setlength\parskip{\baselineskip}
%
\setlength\textwidth{165mm}
\setlength\textheight{160mm} % = 297 - 20 - 117 (hoogte A4 - bottommarge - briefhoofd-hoogte)

% \TeX definitie van \raggedright, de standaarddefinitie werkt niet
\renewcommand\raggedright{\rightskip\z@ plus2em
\spaceskip.3333em
\xspaceskip.5em\relax}

%% PACKAGES LADEN
\RequirePackage{aes}
\RequirePackage{aesfactuur}
\RequirePackage{url}
\RequirePackage{graphicx}
\RequirePackage{ifthen} % ifthen is nu nodig vanwege \newboolean verderop
\RequirePackage{kix}
\RequirePackage{xspace}
% babel staat onderaan OPTIES, vanwege \PassOptionsToPackage aldaar


%% DIVERSE BOOLEANS
\newboolean{aes@optdebug}
\newboolean{aes@optkleur}

\RequirePackage{aestaal}

%% OPTIES
% Laat aestaal de taalopties klaarzetten.
\DeclareTaalOpties

\DeclareOption{kleur}{\setboolean{aes@optkleur}{true}}
\DeclareOption{nokleur}{\setboolean{aes@optkleur}{false}}
\DeclareOption{geenkleur}{\setboolean{aes@optkleur}{false}}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}

\ExecuteOptions{nokleur}
\ProcessOptions*  % *, dus verwerken in door gebruiker opgegeven volgorde (belangrijk voor talen)

% Laat aestaal babel met de gekozen taalopties (of dutch, indien niks opgegeven) laden.
\LaadBabel

%
%
%% LETTERTYPES
\newcommand\aes@fontheader{\fontsize{9}{12}\selectfont\sffamily}
\newcommand\aes@fonttext{\fontsize{10}{12}\selectfont\rmfamily}
\newcommand\aes@fontnaam{\fontsize{11}{12}\selectfont\sffamily\bfseries}
\newcommand\aes@fontadres{\fontsize{13}{15}\selectfont}
%
\renewcommand\familydefault{\rmdefault}
\renewcommand\seriesdefault{m}
\renewcommand\shapedefault{n}
%
\renewcommand\rmdefault{ppl}
\renewcommand\sfdefault{phv}
\renewcommand\ttdefault{cmtt}
%
\renewcommand\bfdefault{b}
\renewcommand\mddefault{m}
%
\renewcommand\itdefault{it}
\renewcommand\sldefault{sl}
\renewcommand\scdefault{sc}
\renewcommand\updefault{n}

\aes@fonttext
%
% geen paginanummers enzo
\pagestyle{empty}

%% STANDAARD INFORMATIE, niet door gebruiker te wijzigen
\newcommand\aes@kvknr{40479641}
\newcommand\aes@NL{\ifthenelse{\boolean{isbuitenland}}{,~NL}{}}
\newcommand\aes@returnline{\aesnaam, Princetonplein~5, 3584~CC~~Utrecht\aes@NL}
\newcommand\aes@bezoekadr{Buys Ballot Lab.\\
                          \TAALkamer~520\\
                          Princetonplein~5\\
                          3584~CC~~Utrecht\aes@NL}
\newcommand\aes@telefoon{+31\,30\,253\,44\,99}
\newcommand\aes@fax{+31\,30\,253\,57\,87}
\newcommand\aes@email{bestuur@a-eskwadraat.nl}
\newcommand\aes@www{www.a-eskwadraat.nl}

\newenvironment{machtiging}{%
	\vspace*{-110mm}\par % weer bovenaan de pagina beginnen
	\begin{minipage}[t]{0.5\textwidth}
	\aes@fontheader Naam\par
	\aes@fonttext \aes@naam
	\bigskip\par
	%
	\aes@fontheader Adres\par
	\aes@fonttext \aes@adres
	\bigskip\par
	%
	\aes@fontheader Postcode + Plaats\par
	\aes@fonttext \aes@postcode
	\bigskip\par
	%
	\vbox to 3cm {
		\aes@fontheader Rekeningnummer\par
		\aes@fonttext \aes@rekeningnummer\par
	}%
	{\LARGE\itshape\bfseries Machtiging}
	\end{minipage}
	\hfill
	\begin{minipage}[t]{41mm}
	\raggedright
	\aesnaam\\
	\aes@bezoekadr\vspace{.9\baselineskip}\par
	\TAALtelefoon:~\aes@telefoon\\
	\TAALfax:~\aes@fax\vspace{.9\baselineskip}\par
	\aes@email\\
	\aes@www\vspace{.9\baselineskip}\par
	\TAALfinancien{\aes@kvknr}{}
	\vspace{.9\baselineskip}\par
	\ifthenelse{\boolean{aes@optkleur}}{\vspace*{33mm}\par}{
	\hspace*{-1cm}\aeslogogrijs[5cm]}
	\end{minipage}
	
	\vspace*{5mm}\par % zorgt voor een goede afstand tot de verdere tekst
	
	Hierbij gaat ondergetekende ermee akkoord dat studievereniging \aesnaam
	volgende bedrag van bovenstaande rekening afschrijft. Het bedrag zal
	jaarlijks in de eerste week van september worden afgeschreven. 
	\vspace{1cm}\par
	\begin{factuur}
}{
	\end{factuur}
	\vfill
	
	\begin{tabular}{p{0.4\textwidth}@{\hspace*{0.1\textwidth}}p{0.4\textwidth}}
	Datum & Handtekening \\
	\vspace*{15mm}~ & \\
	\dotfill & \dotfill
	\end{tabular}
	\vspace{1cm}\par
	
	U kunt het bedrag binnen 30 dagen na afschrijving terug laten boeken op uw
	rekening. Dit kunt u doen via Mijn ING of door contact op te nemen met ING
	Klantenservice (0900-0933). Meer informatie hierover vindt u op
	\url{www.ing.nl}.
	
	Deze machtiging vervalt bij be\"eindiging van uw donateurschap of wanneer u
	deze zelf intrekt. Dit kunt u doen door contact met ons op te nemen via
	bovenstaande contactgegevens.
	\clearpage
}

%%%% inhoud

\newcommand\aes@naam[1]{\PackageError{\filename}{Je hebt geen \protect\naam\space opgegeven.\MessageBreak Deze machtiging moet naar iemand gaan...}{Vul alsnog een \protect\name\space in.}}
\newcommand\aes@adres[1]{\PackageError{\filename}{Je hebt geen \protect\adres\space opgegeven.}{Vul alsnog een \protect\adres\space in.}}
\newcommand\aes@postcode[1]{\PackageError{\filename}{Je hebt geen \protect\postcode\space opgegeven.}{Vul alsnog een \protect\postcode\space in.}}

\newcommand\aes@rekeningnummer[1]{\PackageWarningNoLine{\filename}{Je hebt geen \protect\rekeningnummer\space opgegeven.\MessageBreak Misschien hoeft dit niet, nu puntjes ingevuld.}%
% dit moet standaard wel puntjes krijgen:
\vspace*{\baselineskip}\par
\parbox{2.5cm}{\dotfill}
}

%% GEBRUIKERSCOMMANDO'S

%%%% lay-out parameters
\newcommand\welkleur{\setboolean{aes@optkleur}{true}}
\newcommand\geenkleur{\setboolean{aes@optkleur}{false}}

%%%% INHOUDSPECIFIEKE COMMANDO'S

\newcommand\naam[1]{\renewcommand\aes@naam{#1}}
\newcommand\adres[1]{\renewcommand\aes@adres{#1}}
\newcommand\postcode[1]{\renewcommand\aes@postcode{#1}}
\newcommand\rekeningnummer[1]{\renewcommand\aes@rekeningnummer{#1}}

% mogelijke rekeningen
%BIC- en IBAN-info: http://www.ing.nl/particulier/betalen/betalen-en-het-buitenland/afkortingen-buitenlandbetalingen.aspx
\newcommand\girorekening{%
  \def\aes@bank{ING}%
  \def\aes@giro{656927}%
  \def\aes@BIC{INGBNL2A}%
  \def\aes@IBAN{NL58 INGB 0000656927}%
  \def\aes@rekeninghouder{Studievereniging \aesnaam inz Fiscus}%
}
\girorekening

% en klaar!
\endinput
