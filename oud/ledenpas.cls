% Copyright (C) 2003-2015 TeXniCie A-Eskwadraat <hektex@a-eskwadraat.nl>
%
% This file is part of the A-Eskwadraat LaTeX collection and may be
% redistributed under the terms of version 2 of the GNU General Public License
% as published by the Free Software Foundation. See LICENSE file for details.
%
%% $Id: ledenpas.cls 733 2015-02-23 14:38:43Z aldo $

\NeedsTeXFormat{LaTeX2e}[1994/06/01]
\ProvidesClass{ledenpas}[2000/05/29 v0.1 Het A-Eskwadraat ledenpasje]

\RequirePackage[dutch]{babel}
\RequirePackage[pdftex]{graphicx}
%\def\includegraphics{\@ifnextchar [{\gob@@}{\gob@}}
%\def\gob@@[#1]#2{}
%\def\gob@#1{}
\RequirePackage{uusol}
\RequirePackage{database}
\RequirePackage{tabularx}
%\RequirePackage{graphics}
%\RequirePackage{epsfig}
%\RequirePackage{pifont}
\RequirePackage[utf8]{inputenc}

\LoadClass[a4paper,12pt]{article}

%UPC barcodes
\input ean13
% \X=0.396mm                         % the module size is 120% of standard
%\X=0.6mm                         % the module size is 120% of standard
%\font\ocrb=ocrb7 scaled 1200       % re-load font to the new size
% \font\ocrbsmall=ocrb7 scaled 1200  % re-load font to the new size
%\font\ocrb=pbkl scaled 1000
%\font\ocrbsmall=pbkl scaled 1000
\bcorr=0.015mm                     % new bar correction
%% \ISBN 80-901950-0-8

\voffset-1in
\topmargin=7.5mm
\headheight=0pt
\headsep=0pt
\textheight=282mm
%
\hoffset-1in
\oddsidemargin=5mm
\evensidemargin=5mm
\textwidth=200mm
%
\parindent=0pt
\parskip=0pt
\baselineskip=0pt
\unitlength=0.1mm
%
\pagestyle{empty}

%\setlength{\unitlength}{1mm}
%\font\frut=ecft
%\font\frutb=ecftb
%\font\tekstfont=ecbm scaled 1400
%\font\omschfont=ecbmb scaled 1300
%\font\titelfont=ecbmb scaled 3000

\font\fontachter=pbkl scaled 500
\font\fontjaar=phvb scaled 2500
\font\fontnaam=pbkl scaled 800

%alleen voor nieuwe leden:
%\def\beginstudie{2002}
%\def\fotodir{/home/system/whoswho/leden/feuten_nat/fotosklein/}
%\def\fotodir{/home/system/whoswho/leden/feuten/}

\def\fotodir{/export/fotos/Private/}
%\def\buttfoto{Schildpad_Cornelis.png}
\def\aes{\hbox{A--Es}\-kwa\-draat}
\def\achtertekst{%
Er kan bij A--Eskwadraat activiteiten en bij de A--Eskwadraat boekverkoop
gevraagd worden naar deze pas. Bewaar hem daarom goed.
%\begin{itemize}%
%\itemsep=-\parsep
%\vskip-\parsep
%\vskip-\parsep
%\vskip-\parsep
% \item aan alle A--Eskwadraatactiviteiten deel te nemen
% \item boeken bij de A--Eskwadraat boekverkoop te bestellen en te kopen
% %\item tegen gereduceerd tarief meedoen aan culturele activiteiten.
%\end{itemize}
%\vskip-\parsep
%\vskip-\parsep
%\vskip-\parsep
%\vskip-\parsep
\vskip0mm
\begin{tabular}{lr}
Studievereniging A--Eskwadraat & Voor meer informatie \\
Princetonplein 5 & bezoek onze homepage \\
3584 CC  Utrecht & \texttt{http://www.A-Eskwadraat.nl} \\
030--253 4499 & of mail ons: \texttt{info@A-Eskwadraat.nl}
\end{tabular}
}
\edef\leeg{}

\newcount\kol
\newcount\rij
\newcount\ypos
\newcount\xpos

\def\@voorkantgegevens{%
\put(35,-365){\includegraphics[width=25mm,height=33mm]{\fotonaam}}%
\put(325,-100){\makebox(400,50)[l]{\fontjaar\jaar}}%
\put(325,-175){\makebox(400,50)[l]{\fontnaam\regela}}%
\put(325,-225){\makebox(400,50)[l]{\fontnaam\regelb}}%
\put(325,-275){\makebox(400,50)[l]{\fontnaam\regelc}}%
\put(325,-325){\makebox(400,50)[l]{\fontnaam\regeld}}%
\put(325,-375){\makebox(400,50)[l]{\fontnaam\regele}}%
%\put(325,-225){\makebox(400,50)[l]{\fontnaam\helenaam{}}}%
%%\put(325,-275){\makebox(400,50)[l]{\fontnaam\kortstudieafk{}}}%\beginstudie
%\put(325,-275){\makebox(400,50)[l]{\fontnaam\studie}}% \beginstudie
%\put(325,-325){\makebox(400,50)[l]{\fontnaam{}Lidnr: \lidnr}}%
}

\def\@voorformulierpasje{%
\global\def\@vulin{%
  \put(0,-450){\includegraphics[width=80mm]{voorkant-\jaar.png}}%
  \@voorkantgegevens%
}%
\@tekenvoorkant}

\def\@voorformulierpasjealleentekst{%
\let\@vulin\@voorkantgegevens%
\@tekenvoorkant}

\def\@voorformulierleegpasje{%
\global\def\@vulin{\put(0,-450){\includegraphics[width=80mm]{voorkant-\jaar.png}}}%
\let\foto\relax%
\let\voornaam\relax%
\let\voorletters\relax%
\@tekenvoorkant}

\def\@tekenvoorkant{%
\ifnum\kol=0
  \xpos=100%
\else
  \xpos=1100%
\fi%
\ypos=5
\advance\ypos by-\rij%
\multiply\ypos by-460%
%
%\ifx\leeg\foto
	%\edef\foto{\buttfoto}%
%\fi%
%
%\ifx\leeg\voornaam
	%\edef\voornaam{\voorletters}%
%\fi%
%
\ifx\relax\fotonaam \else%
  \edef\fotonaam{\fotodir\foto}%
\fi%
%
%definitie \kortstudieafk is naar einde verplaatst
%
\unitlength=.1mm%
\begin{picture}(0,0)(-\xpos,-\ypos)
\@vulin%
%\put(0,0){\line(1,0){800}}
%\put(800,0){\line(0,-1){450}}
%\put(800,-450){\line(-1,0){800}}
%\put(0,-450){\line(0,1){450}}
\end{picture}%
%
%Tellers ophogen
\advance\kol by1
\ifnum\kol=2
  \global\advance\rij by1
  \global\kol=0
\fi%
\ifnum\rij=6
%  \end{picture}%
  \clearpage%
  \global\rij=0%
\fi%
}

\def\@achterkantgegevens{%
\put(40,-90){\makebox(720,54)[tl]{\parbox{72mm}%
 {\baselineskip=.4\baselineskip\tiny\fontachter\achtertekst}}}%
\put(40,-245){\makebox(720,64)[tr]{\parbox{72mm}%
 {\center \barheight=1cm ~\expandafter\EAN\eancode~}}}%
}

\def\@achterformulierpasje{%
\global\def\@vulin{%
  \put(0,-450){\includegraphics[width=80mm]{achterkant.png}}%
  \@achterkantgegevens%
}%
\@tekenachterkant}

\def\@achterformulierpasjealleentekst{%
\let\@vulin\@achterkantgegevens%
\@tekenachterkant}

\def\@achterformulierleegpasje{%
\global\def\@vulin{\put(0,-450){\includegraphics[width=80mm]{achterkant.png}}}%
\@tekenachterkant}

\def\@tekenachterkant{%
\ifnum\kol=0
  \xpos=1100%
\else
  \xpos=100%
\fi%
\ypos=5
\advance\ypos by-\rij%
\multiply\ypos by-460%
%
\unitlength=.1mm%
\begin{picture}(0,0)(-\xpos,-\ypos)
\@vulin
%
%\put(0,0){\line(1,0){800}}
%\put(800,0){\line(0,-1){450}}
%\put(800,-450){\line(-1,0){800}}
%\put(0,-450){\line(0,1){450}}
\end{picture}%
%
%Tellers ophogen
\advance\kol by1
\ifnum\kol=2
  \global\advance\rij by1
  \global\kol=0
\fi%
\ifnum\rij=6
%  \end{picture}%
  \clearpage%
  \global\rij=0%
\fi%
}

\def\leesfile#1{%
\let\@tekenvoorkantpas\@voorformulierpasje%
\let\@tekenachterkantpas\@achterformulierpasje%
\@tekenpasjesuitfile{#1}%
}

\def\leesfilealleentekst#1{%
\let\@tekenvoorkantpas\@voorformulierpasjealleentekst%
\let\@tekenachterkantpas\@achterformulierpasjealleentekst%
\@tekenpasjesuitfile{#1}%
}

\def\@tekenpasjesuitfile#1{%
\global\rij=0
\global\kol=0
\begin{database}{#1}{|}
%intro data.csv
%{id:mentor:studie:voornaam:achternaam:tussenvoegsels:voorletters:gebdat:geslacht:straat:huisno:postcode:woonplaats:telefoon:mobi:ouderstel:ouderstel2:studnr:lidnr:email:foto:homepage:icq:nick}
%resultaat.csv uit w=w2
{eancode|jaar|foto|regela|regelb|regelc|regeld|regele}
  \@tekenvoorkantpas
\end{database}%
\clearpage
\global\rij=0
\global\kol=0
\begin{database}{#1}{|}
%intro data.csv
%{id:mentor:studie:voornaam:achternaam:tussenvoegsels:voorletters:gebdat:geslacht:straat:huisno:postcode:woonplaats:telefoon:mobi:ouderstel:ouderstel2:studnr:lidnr:email:foto:homepage:icq:nick}
%resultaat.csv uit w=w2
{eancode|jaar|foto|regela|regelb|regelc|regeld|regele}
  \@tekenachterkantpas
\end{database}%
}

\def\maaklegepasjes{%
\global\rij=0
\global\kol=0
\@voorformulierleegpasje\@voorformulierleegpasje%
\@voorformulierleegpasje\@voorformulierleegpasje%
\@voorformulierleegpasje\@voorformulierleegpasje%
\@voorformulierleegpasje\@voorformulierleegpasje%
\@voorformulierleegpasje\@voorformulierleegpasje%
\@voorformulierleegpasje\@voorformulierleegpasje%
\clearpage
\global\rij=0
\global\kol=0
\@achterformulierleegpasje\@achterformulierleegpasje%
\@achterformulierleegpasje\@achterformulierleegpasje%
\@achterformulierleegpasje\@achterformulierleegpasje%
\@achterformulierleegpasje\@achterformulierleegpasje%
\@achterformulierleegpasje\@achterformulierleegpasje%
\@achterformulierleegpasje\@achterformulierleegpasje%
}

%\def\bali{ }\def\balii{}


%\def\kortstudieafi#1#2#3#4.{#1#2#3}%
%\def\Ste{Ste}\def\Tec{Tec}\def\Med{Med}\def\Com{Com}\def\Inf{Inf}\def\TWI{TWI}%
%\def\Wis{Wis}\def\Met{Met}\def\Nat{Nat}%
%\def\ifinf#1de#2.{\ifx\relax#2\expandafter\ifinfi\studie nt\relax.%
%  \else IK\fi}%
%\def\ifinfi#1nt#2.{\ifx\relax#2Informatica\else I\&M\fi}%
%\def\ifTWIN#1FO#2.{\ifx\relax#2\expandafter\ifTWINi\studie NS\relax.%
%  \else Twinfo\fi}%
%\def\ifTWINi#1NS#2.{\ifx\relax#2TwNW\else TwinNS\fi}%
%\def\ifWEB#1jf#2.{\ifx\relax#2Wiskunde\else WEB\fi}%
%\def\kortstudieafk{%
%  \ifx\studie\bali Bal\else
%  \ifx\studie\balii Bal\else
%    \edef\afk{\expandafter\kortstudieafi\studie.}%
%    \ifx\afk\Ste Sterrenkunde\else
%    \ifx\afk\Met MFO\else
%    \ifx\afk\Tec TKI\else
%    \ifx\afk\Med�MTI\else
%    \ifx\afk\Com CS\else
%    \ifx\afk\Inf
%      \expandafter\ifinf\studie de\relax.\else
%    \ifx\afk\TWI
%      \expandafter\ifTWIN\studie FO\relax.\else
%    \ifx\afk\Wis
%      \expandafter\ifWEB\studie jf\relax.%
%    \else\studie\fi\fi\fi\fi\fi\fi\fi\fi
%  \fi\fi}%

% vim: syntax=tex
