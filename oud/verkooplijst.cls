% Copyright (C) 1999-2015 TeXniCie A-Eskwadraat <hektex@a-eskwadraat.nl>
%
% This file is part of the A-Eskwadraat LaTeX collection and may be
% redistributed under the terms of version 2 of the GNU General Public License
% as published by the Free Software Foundation. See LICENSE file for details.
%
%% $Id: verkooplijst.cls 733 2015-02-23 14:38:43Z aldo $

\def\filename{verkooplijst}
\def\filedate{2003/11/25}
\def\fileversion{1.01}
\NeedsTeXFormat{LaTeX2e}[1999/06/01]
\ProvidesClass{\filename}[\filedate\space v\fileversion\space A-Eskwadraat
verkooplijst class]

\LoadClass[10pt,a4paper]{article}
\RequirePackage{aes,eurosym,longtable}
\RequirePackage[dutch]{babel}
\RequirePackage[utf8]{inputenc}
\pagestyle{empty}
\advance\textheight by 5.5cm
\advance\textwidth by 2.0cm
\advance\voffset by -31mm
\advance\hoffset by -3.5cm
\unitlength1cm
\def\inputfile{/home/bestuur/boekcom/tmp/maptmp.tex}
\def\eur{\euro{}}
\def\boekenleeg{%
&&&&&&\\
\hline}

\def\kastotaal{%
\begin{center}
  \begin{tabular}{|l|p{20mm}|l|p{20mm}|}
    \hline
    \multicolumn{2}{|c|}{Tellingen} &
    \multicolumn{2}{|c|}{Boekingen}\\
    \hline
    Pin: & ~ & Totaal \eur$_{tot}$: & ~\\
    &&&\\
    \hline
    &&&\\
    &&&\\
    \hline
    \hline
    Totaal tellingen: & ~ & Totaal boekingen: & ~\\
    &&&\\
    \hline
  \end{tabular}
  \end{center}
}

\def\boekentabel{%
\setlongtables
  \begin{longtable}{|l|p{90mm}|r|c|c|p{10mm}|p{10mm}|}
    \hline
    ISBN & Auteur, \hfill Titel& {Prijs}&$\#_{vrrd}$&$\#_{btnvrkp}$&$\#_{verk}$&\eur$_{tot}$\\
    \endhead
    \multicolumn{7}{|c|}{Z.O.Z}\\
    \hline
    \endfoot
    \hline
    \endlastfoot
    \hline
    \hline
    \input{\inputfile}
    \boekenleeg
    \boekenleeg
    \boekenleeg
    \boekenleeg
    \boekenleeg
    \multicolumn{6}{|r|}{Totaal \euro}&\\
    \hline
  \end{longtable}
}

\newenvironment{lijst}{%
  \small
  \begin{center}
  {\noindent Datum: \today \hspace{15mm} Verkoper: \hspace{2cm} Lijst bijgewerkt tot: \today}
  \end{center}

  \boekentabel

  \bigskip

  \kastotaal
}{}
\endinput
