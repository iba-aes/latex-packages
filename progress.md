# A-Eskwadraat package modernisation progression

Here, we keep track of the progress of the modernisation of the A-Eskwadraat packages

## To be included in the dtx

- [x] `aes.sty`
- [x] `aesfactuur.sty`
- [x] `AUTHORS`
- [x] `beamerthemeaes2.sty`
- [x] `notulen.cls`
- [x] `aestaal.sty`
- [x] `aesbrief.cls`

## To be excluded from the dtx

- `aanmeldingsform.cls`
- `activiteiten.cls`
- `advertorial.sty`
- `card.cls`
- `contract.cls`
- `ledenpas.cls`
- `qrcode.sty`

## Undecided (likely not)
- `aesfactuurbtw.sty`
- `aesposter.cls`
- `aesroots.cls`
- `aesrootsgroot.cls`
- `atbeginend.sty`
- `avnotulen.sty`
- `bo.cls`
- `bo.sty`
- `boekje.cls`
- `bonnen.sty`
- `btw.cls`
- `bundel.cls`
- `certificaat.cls`
- `cursus.sty`
- `database.sty`
- `donabrief.cls`
- `etiketten.cls`
- `eubegroot.sty`
- `eunota.cls`
- `ht-vakid.cls`
- `ibaverklaart.cls`
- `machtiging.cls`
- `machtiging.sty`
- `stiekem.sty`
- `stiekemext.sty`
- `tbc.sty`
- `tbcpdf.sty`
- `verkooplijst.cls`
- `aesroots.cls`
- `vakidioot.cls`