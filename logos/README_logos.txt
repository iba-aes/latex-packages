The files in this folder are not covered by the project's license.

The files aeskwadraat_BW.pdf, aeskwadraat_RGB.pdf and aeskwadraat_slinger.pdf are property of A-Eskwadraat. They are released under the CC BY-SA 4.0 license.

The Utrecht University logo, aeskwadraat_uulogo.jpg, is owned by Utrecht University and may only be used by affiliates of the university.